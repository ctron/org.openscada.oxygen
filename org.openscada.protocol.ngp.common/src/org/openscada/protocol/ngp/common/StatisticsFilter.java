/*
 * This file is part of the openSCADA project
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.protocol.ngp.common;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.apache.mina.core.filterchain.IoFilterAdapter;
import org.apache.mina.core.session.AttributeKey;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.write.WriteRequest;
import org.openscada.core.info.StatisticsImpl;

public class StatisticsFilter extends IoFilterAdapter
{
    public static final AttributeKey STATS_KEY = new AttributeKey ( StatisticsFilter.class, "stats" );

    private final static Object STATS_READ_BYTES = new Object ();

    private final static Object STATS_WRITTEN_BYTES = new Object ();

    private final static Object STATS_SCHEDULED_WRITE_BYTES = new Object ();

    private final static Object STATS_READ_BYTES_THROUGHPUT = new Object ();

    private final static Object STATS_WRITTEN_BYTES_THROUGHPUT = new Object ();

    public static final String DEFAULT_NAME = "core.stats";

    private final Set<StatisticsImpl> statsMapper = Collections.synchronizedSet ( new HashSet<StatisticsImpl> () );

    @Override
    public void sessionClosed ( final NextFilter nextFilter, final IoSession session ) throws Exception
    {
        this.statsMapper.remove ( session );
        super.sessionClosed ( nextFilter, session );
    }

    private StatisticsImpl getStats ( final IoSession session )
    {
        final Object o = session.getAttribute ( StatisticsFilter.STATS_KEY );
        if ( o instanceof StatisticsImpl )
        {
            final StatisticsImpl stats = (StatisticsImpl)o;
            if ( this.statsMapper.add ( stats ) )
            {
                init ( stats );
            }
            return stats;
        }
        else
        {
            return null;
        }
    }

    private void init ( final StatisticsImpl stats )
    {
        stats.setLabel ( STATS_READ_BYTES, "Read bytes" );
        stats.setLabel ( STATS_WRITTEN_BYTES, "Written bytes" );
        stats.setLabel ( STATS_SCHEDULED_WRITE_BYTES, "Scheduled write bytes" );
        stats.setLabel ( STATS_READ_BYTES_THROUGHPUT, "Read bytes throughput" );
        stats.setLabel ( STATS_WRITTEN_BYTES_THROUGHPUT, "Written bytes throughput" );
    }

    @Override
    public void filterWrite ( final NextFilter nextFilter, final IoSession session, final WriteRequest writeRequest ) throws Exception
    {
        updateStats ( session );

        super.filterWrite ( nextFilter, session, writeRequest );
    }

    @Override
    public void messageReceived ( final NextFilter nextFilter, final IoSession session, final Object message ) throws Exception
    {
        updateStats ( session );

        super.messageReceived ( nextFilter, session, message );
    }

    private void updateStats ( final IoSession session )
    {
        final StatisticsImpl stats = getStats ( session );

        if ( stats != null )
        {
            stats.setCurrentValue ( STATS_READ_BYTES, session.getReadBytes () );
            stats.setCurrentValue ( STATS_WRITTEN_BYTES, session.getWrittenBytes () );
            stats.setCurrentValue ( STATS_SCHEDULED_WRITE_BYTES, session.getScheduledWriteBytes () );
            stats.setCurrentValue ( STATS_READ_BYTES_THROUGHPUT, session.getReadBytesThroughput () );
            stats.setCurrentValue ( STATS_WRITTEN_BYTES_THROUGHPUT, session.getWrittenBytesThroughput () );
        }
    }

    @Override
    public void messageSent ( final NextFilter nextFilter, final IoSession session, final WriteRequest writeRequest ) throws Exception
    {
        updateStats ( session );

        super.messageSent ( nextFilter, session, writeRequest );
    }
}
