/*
 * This file is part of the openSCADA project
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.protocol.ngp.common.mc.protocol.serialize;

import java.io.NotSerializableException;
import java.io.Serializable;

import org.apache.mina.core.buffer.IoBuffer;
import org.openscada.protocol.ngp.common.mc.message.DataMessage;
import org.openscada.protocol.ngp.common.mc.protocol.MessageDecoder;
import org.openscada.protocol.ngp.common.mc.protocol.MessageEncoder;
import org.openscada.protocol.ngp.common.mc.protocol.MessageProtocol;

public class ObjectSerializationProtocol implements MessageProtocol, MessageEncoder, MessageDecoder
{

    private final ClassLoader classLoader;

    public ObjectSerializationProtocol ()
    {
        this.classLoader = Thread.currentThread ().getContextClassLoader ();
    }

    public ObjectSerializationProtocol ( final ClassLoader classLoader )
    {
        this.classLoader = classLoader;
    }

    @Override
    public MessageEncoder getEncoder ()
    {
        return this;
    }

    @Override
    public MessageDecoder getDecoder ()
    {
        return this;
    }

    @Override
    public Object decodeMessage ( final DataMessage message ) throws Exception
    {
        return message.getData ().getObject ( this.classLoader );
    }

    @Override
    public DataMessage encodeMessage ( final Object message ) throws Exception
    {
        if ( ! ( message instanceof Serializable ) )
        {
            if ( message != null )
            {
                throw new NotSerializableException ( message.getClass ().getName () );
            }
            else
            {
                throw new NotSerializableException ();
            }
        }

        final IoBuffer data = IoBuffer.allocate ( 64 );
        data.setAutoExpand ( true );
        data.putObject ( message );
        data.flip ();
        return new DataMessage ( data );
    }
}
