/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.protocol.ngp.common.mc.frame;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoder;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

public class FrameEncoder implements ProtocolEncoder
{

    public FrameEncoder ()
    {
    }

    @Override
    public void dispose ( final IoSession session ) throws Exception
    {
    }

    @Override
    public void encode ( final IoSession session, final Object message, final ProtocolEncoderOutput output ) throws Exception
    {
        if ( ! ( message instanceof Frame ) )
        {
            throw new IllegalStateException ( String.format ( "Can only encode messages of type Frame but got %s", message.getClass () ) );
        }

        final Frame frame = (Frame)message;

        final IoBuffer buffer = IoBuffer.allocate ( 1 + 1 + 4 + frame.getData ().remaining () );
        buffer.put ( (byte)0x01 ); // version - #0
        buffer.put ( (byte)frame.getType ().ordinal () ); // frame type - #1
        buffer.putInt ( frame.getData ().remaining () ); // data size - #2
        buffer.put ( frame.getData () ); // data - #6
        buffer.flip ();

        output.write ( buffer );
    }
}