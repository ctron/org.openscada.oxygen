/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.protocol.ngp.common.mc.frame;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.openscada.protocol.ngp.common.mc.frame.Frame.FrameType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FrameDecoder extends CumulativeProtocolDecoder
{

    private final static Logger logger = LoggerFactory.getLogger ( FrameDecoder.class );

    public FrameDecoder ()
    {
    }

    @Override
    protected boolean doDecode ( final IoSession session, final IoBuffer data, final ProtocolDecoderOutput output ) throws Exception
    {
        logger.trace ( "decode data - session: {}, data: {}", session, data );

        if ( data.remaining () < 6 )
        {
            return false;
        }

        final int dataLength = data.getInt ( data.position () + 2 ); // we need to look at "here" + 2

        logger.trace ( "Data length: {}, remainingData: {}", dataLength, data.remaining () - 6 );

        if ( data.remaining () < 6 + dataLength )
        {
            return false;
        }

        final byte version = data.get (); // version - #0
        if ( version != 0x01 )
        {
            throw new IllegalStateException ( String.format ( "Version 0x%02x is not supported.", version ) );
        }

        final int frameTypeOrdinal = data.get (); // frame type - #1
        data.getInt (); // dataLength - #2

        // data - #6
        final IoBuffer frameData = data.getSlice ( dataLength );

        final Frame frame = new Frame ( FrameType.values ()[frameTypeOrdinal], frameData );

        logger.trace ( "Decoded frame: {} ... {} bytes remaining", frame, data.remaining () );
        output.write ( frame );

        return true;
    }
}