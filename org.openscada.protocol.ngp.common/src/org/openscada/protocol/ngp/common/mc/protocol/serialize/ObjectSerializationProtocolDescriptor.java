/*
 * This file is part of the openSCADA project
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.protocol.ngp.common.mc.protocol.serialize;

import org.apache.mina.core.session.IoSession;
import org.openscada.protocol.ngp.common.mc.DataMessageFilter;
import org.openscada.protocol.ngp.common.mc.protocol.ProtocolDescriptor;

public class ObjectSerializationProtocolDescriptor implements ProtocolDescriptor
{

    private final ClassLoader classLoader;

    private final String protocolId;

    public ObjectSerializationProtocolDescriptor ( final String protocolId, final ClassLoader classLoader )
    {
        this.protocolId = protocolId == null ? "java" : protocolId;
        this.classLoader = classLoader == null ? Thread.currentThread ().getContextClassLoader () : classLoader;
    }

    @Override
    public String getProtocolId ()
    {
        return this.protocolId;
    }

    @Override
    public void activate ( final IoSession session )
    {
        final DataMessageFilter filter = new DataMessageFilter ( new ObjectSerializationProtocol ( this.classLoader ) );
        session.getFilterChain ().addLast ( "dataMessage", filter );
    }

}
