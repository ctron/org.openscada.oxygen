/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.protocol.ngp.common.mc.handshake;

import java.util.Map;

import org.apache.mina.filter.compression.CompressionFilter;
import org.openscada.protocol.ngp.common.ChainConfigurator;
import org.openscada.protocol.ngp.common.ProtocolConfiguration;
import org.openscada.protocol.ngp.common.mc.message.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StreamCompressionHandshake extends AbstractHandshake
{

    private final static Logger logger = LoggerFactory.getLogger ( StreamCompressionHandshake.class );

    private final Integer maxStreamCompressionLevel;

    public StreamCompressionHandshake ( final Integer maxStreamCompressionLevel )
    {
        this.maxStreamCompressionLevel = Math.min ( maxStreamCompressionLevel, CompressionFilter.COMPRESSION_MAX );
    }

    @Override
    public void request ( final HandshakeContext handshakeContext, final Map<String, String> helloProperties )
    {
        if ( Boolean.getBoolean ( "org.openscada.protocol.ngp.common.mc.handshake.disableCompression" ) )
        {
            logger.info ( "Stream compression disabled by system property (org.openscada.protocol.ngp.common.mc.handshake.disableCompression)" );
            return;
        }

        final ProtocolConfiguration protocolConfiguration = handshakeContext.getProtocolConfiguration ();
        logger.debug ( "Stream compression in protocol configuration: {}", protocolConfiguration.getStreamCompressionLevel () );

        if ( protocolConfiguration.getStreamCompressionLevel () == -1 || protocolConfiguration.getStreamCompressionLevel () > 0 )
        {
            final String value = String.format ( "%s", protocolConfiguration.getStreamCompressionLevel () );
            logger.info ( "Requesting stream compression level = {}", value );
            helloProperties.put ( Constants.PROP_STREAM_COMPRESSION_LEVEL, value );
        }
    }

    @Override
    public void handshake ( final HandshakeContext handshakeContext, final Map<String, String> helloProperties, final Map<String, String> acceptedProperties ) throws Exception
    {
        final Integer streamCompressionLevel = performHandshake ( helloProperties, acceptedProperties );
        logger.info ( "Setting stream compression to: {}", streamCompressionLevel );

        if ( streamCompressionLevel != null )
        {
            acceptedProperties.put ( Constants.PROP_STREAM_COMPRESSION_LEVEL, "" + streamCompressionLevel );
        }
    }

    protected Integer performHandshake ( final Map<String, String> helloProperties, final Map<String, String> acceptedProperties )
    {
        final Integer streamCompressionLevel = getInteger ( helloProperties, Constants.PROP_STREAM_COMPRESSION_LEVEL, null );

        logger.debug ( "Stream compression level settings - ours: {}, theirs: {}", this.maxStreamCompressionLevel, streamCompressionLevel );

        if ( streamCompressionLevel == null )
        {
            // client does not support stream compression
            return null;
        }
        if ( this.maxStreamCompressionLevel == null )
        {
            // server does not support stream compression
            return null;
        }

        if ( streamCompressionLevel < 0 )
        {
            // the server may fully decide
            return this.maxStreamCompressionLevel;
        }
        else if ( this.maxStreamCompressionLevel < 0 )
        {
            // the client may fully decide
            return streamCompressionLevel;
        }
        else
        {
            return Math.min ( this.maxStreamCompressionLevel, streamCompressionLevel );
        }
    }

    @Override
    public void apply ( final HandshakeContext handshakeContext, final Map<String, String> acceptedProperties ) throws Exception
    {
        final Integer streamCompressionLevel = getInteger ( acceptedProperties, Constants.PROP_STREAM_COMPRESSION_LEVEL, null );
        if ( streamCompressionLevel != null )
        {
            new ChainConfigurator ( handshakeContext.getSession () ).startStreamCompression ( streamCompressionLevel, false );
        }
    }

    @Override
    public void postApply ( final HandshakeContext context, final Map<String, String> acceptedProperties ) throws Exception
    {
    }

    @Override
    public void sessionStarted ( final HandshakeContext context, final Map<String, String> acceptedProperties ) throws Exception
    {
    }
}
