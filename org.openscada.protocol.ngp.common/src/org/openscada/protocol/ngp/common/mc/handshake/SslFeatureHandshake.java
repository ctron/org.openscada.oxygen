/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.protocol.ngp.common.mc.handshake;

import java.util.Map;

import org.openscada.protocol.ngp.common.ChainConfigurator;

public class SslFeatureHandshake extends AbstractHandshake
{

    @Override
    public void request ( final HandshakeContext context, final Map<String, String> helloProperties )
    {
        if ( context.getProtocolConfiguration ().getSslContextFactory () != null )
        {
            helloProperties.put ( "requestSsl", "true" );
        }
    }

    @Override
    public void handshake ( final HandshakeContext context, final Map<String, String> helloProperties, final Map<String, String> acceptedProperties ) throws Exception
    {
        if ( context.getProtocolConfiguration ().getSslContextFactory () == null )
        {
            // we cannot do SSL ... client may choose to close
            return;
        }

        if ( !getBoolean ( helloProperties, "requestSsl", Boolean.FALSE ) )
        {
            // SSL was not requested
            return;
        }

        // we can do ssl and it was requested
        acceptedProperties.put ( "useSsl", "true" );
    }

    @Override
    public void apply ( final HandshakeContext context, final Map<String, String> acceptedProperties ) throws Exception
    {
        if ( !getBoolean ( acceptedProperties, "useSsl", Boolean.FALSE ) )
        {
            return;
        }
        /*
         * If we start ssl and are in server mode the first packet needs to be sent outside of ssl.
         * In client mode the next (first) packet will already be SSL.
         */
        new ChainConfigurator ( context.getSession () ).startSsl ( !context.isClientMode (), false );
    }

    @Override
    public void postApply ( final HandshakeContext context, final Map<String, String> acceptedProperties ) throws Exception
    {
    }

    @Override
    public void sessionStarted ( final HandshakeContext context, final Map<String, String> acceptedProperties ) throws Exception
    {
    }

}
