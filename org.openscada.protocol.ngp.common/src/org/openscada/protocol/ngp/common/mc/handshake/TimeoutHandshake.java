/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.protocol.ngp.common.mc.handshake;

import java.util.Map;

import org.openscada.protocol.ngp.common.ChainConfigurator;
import org.openscada.protocol.ngp.common.mc.MessageChannelFilter;
import org.openscada.protocol.ngp.common.mc.message.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TimeoutHandshake extends AbstractHandshake
{

    private final static Logger logger = LoggerFactory.getLogger ( TimeoutHandshake.class );

    @Override
    public void request ( final HandshakeContext handshakeContext, final Map<String, String> helloProperties )
    {
        helloProperties.put ( Constants.PROP_TIMEOUT, "" + handshakeContext.getProtocolConfiguration ().getTimeout () );
    }

    @Override
    public void handshake ( final HandshakeContext handshakeContext, final Map<String, String> helloProperties, final Map<String, String> acceptedProperties ) throws Exception
    {
        final Integer timeout = getInteger ( helloProperties, Constants.PROP_TIMEOUT, null );
        if ( timeout != null )
        {
            final int newTimeout = Math.min ( Math.max ( timeout, MessageChannelFilter.MIN_TIMEOUT ), MessageChannelFilter.MAX_TIMEOUT );
            logger.info ( "Timeout setting = {}", newTimeout );
            acceptedProperties.put ( Constants.PROP_TIMEOUT, "" + newTimeout );
        }
    }

    @Override
    public void apply ( final HandshakeContext handshakeContext, final Map<String, String> acceptedProperties ) throws Exception
    {
    }

    @Override
    public void postApply ( final HandshakeContext handshakeContext, final Map<String, String> acceptedProperties ) throws Exception
    {
    }

    @Override
    public void sessionStarted ( final HandshakeContext handshakeContext, final Map<String, String> acceptedProperties ) throws Exception
    {
        // the pinging is started when the session got opened
        final Integer timeout = getInteger ( acceptedProperties, Constants.PROP_TIMEOUT, null );

        if ( timeout != null )
        {
            final int pingFrequency = 3;
            new ChainConfigurator ( handshakeContext.getSession () ).startKeepAlive ( pingFrequency, (int)Math.ceil ( timeout / 1000.0 ) );
            return;
        }

        new ChainConfigurator ( handshakeContext.getSession () ).startKeepAlive ( handshakeContext.getProtocolConfiguration ().getPingFrequency (), (int)Math.ceil ( handshakeContext.getProtocolConfiguration ().getTimeout () / 1000.0 ) );
    }
}
