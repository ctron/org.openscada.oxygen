/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.protocol.ngp.common.mc.handshake;

import java.util.Map;

import org.openscada.protocol.ngp.common.mc.message.AcceptMessage;
import org.openscada.protocol.ngp.common.mc.message.HelloMessage;
import org.openscada.protocol.ngp.common.mc.message.StartMessage;

/**
 * A handshake module which will take part on the message channel setup
 * procedure
 * 
 * @author Jens Reimann
 */
public interface Handshake
{
    /**
     * Called by the client, putting together all properties for the
     * {@link HelloMessage}
     * 
     * @param context
     *            the handshake context
     * @param helloProperties
     *            the properties to fill
     */
    public void request ( HandshakeContext context, Map<String, String> helloProperties );

    /**
     * Called by the server and actually performing the handshake based on the
     * helloProperties from the client
     * <p>
     * The server needs to evaluate what the client requested and puts the
     * result into the acceptedProperties. No action will be done right now. The
     * result in the acceptedProperties will also be sent to the client.
     * </p>
     * 
     * @param context
     *            the handshake context
     * @param helloProperties
     *            the properties from the client
     * @param acceptedProperties
     *            the properties to fill with the result of the handshake
     * @throws Exception
     *             if anything goes wrong
     */
    public void handshake ( HandshakeContext context, Map<String, String> helloProperties, Map<String, String> acceptedProperties ) throws Exception;

    /**
     * Activate the results from the previous handshake run. This is called on
     * the server directly and on the client when the {@link AcceptMessage} was
     * received.
     * <p>
     * In this phase all handshake modules should apply the changes needed. They
     * may also change the sessions filter chain to change the way it
     * communicates.
     * </p>
     * 
     * @param context
     *            the handshake context
     * @param acceptedProperties
     *            the accepted properties from the servers handshake run
     * @throws Exception
     *             if anything goes wrong
     */
    public void apply ( HandshakeContext context, Map<String, String> acceptedProperties ) throws Exception;

    /**
     * A second apply run performed directly after the run to
     * {@link #apply(HandshakeContext, Map)}
     * <p>
     * This second apply run has the possibility to perform stuff which needs to
     * be done after all changes to the filter change have been performed. So
     * messages sent in this phase will already be encoded correctly.
     * </p>
     * 
     * @param context
     *            the handshake context
     * @param acceptedProperties
     *            the accepted properties from the servers handshake run
     * @throws Exception
     *             if anything goes wrong
     */
    public void postApply ( HandshakeContext context, Map<String, String> acceptedProperties ) throws Exception;

    /**
     * Called on the server and client when the session got started.
     * <p>
     * For the server this may either be directly after the handshake run or
     * later when the {@link StartMessage} has been received. This depends on
     * the fact weather the client supports the session start command.
     * </p>
     * <p>
     * For the client this is after the session has been accepted
     * </p>
     * 
     * @param context
     *            the handshake context
     * @param acceptedProperties
     *            the accepted properties from the servers handshake run
     * @throws Exception
     *             if anything goes wrong
     */
    public void sessionStarted ( HandshakeContext context, Map<String, String> acceptedProperties ) throws Exception;
}
