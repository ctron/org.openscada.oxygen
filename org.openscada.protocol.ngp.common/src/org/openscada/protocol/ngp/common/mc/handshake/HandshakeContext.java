/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.protocol.ngp.common.mc.handshake;

import org.apache.mina.core.filterchain.IoFilter.NextFilter;
import org.apache.mina.core.session.IoSession;
import org.openscada.protocol.ngp.common.ProtocolConfiguration;

public class HandshakeContext
{
    private final ProtocolConfiguration protocolConfiguration;

    private final boolean clientMode;

    private final IoSession session;

    private final NextFilter nextFilter;

    public HandshakeContext ( final ProtocolConfiguration protocolConfiguration, final boolean clientMode, final IoSession session, final NextFilter nextFilter )
    {
        super ();
        this.protocolConfiguration = protocolConfiguration;
        this.clientMode = clientMode;
        this.session = session;
        this.nextFilter = nextFilter;
    }

    public ProtocolConfiguration getProtocolConfiguration ()
    {
        return this.protocolConfiguration;
    }

    public boolean isClientMode ()
    {
        return this.clientMode;
    }

    public IoSession getSession ()
    {
        return this.session;
    }

    public NextFilter getNextFilter ()
    {
        return this.nextFilter;
    }

}
