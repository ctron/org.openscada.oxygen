/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.protocol.ngp.common.mc.frame;

import org.apache.mina.core.buffer.IoBuffer;

public class Frame
{
    public static enum FrameType
    {
        HELLO,
        MESSAGE,
        ACCEPT,
        CLOSE,
        START,
        PING,
        PONG;
    }

    private final FrameType type;

    private final IoBuffer data;

    public Frame ( final FrameType type )
    {
        this ( type, IoBuffer.allocate ( 0 ) );
    }

    public Frame ( final FrameType type, final IoBuffer data )
    {
        this.type = type;
        this.data = data;
    }

    public IoBuffer getData ()
    {
        return this.data;
    }

    public FrameType getType ()
    {
        return this.type;
    }

    @Override
    public String toString ()
    {
        return String.format ( "[Frame: %s - %s]", this.type, this.data );
    }
}
