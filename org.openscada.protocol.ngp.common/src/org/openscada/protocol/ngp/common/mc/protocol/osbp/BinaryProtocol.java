/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.protocol.ngp.common.mc.protocol.osbp;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.apache.mina.core.buffer.IoBuffer;
import org.openscada.protocol.ngp.common.mc.message.DataMessage;
import org.openscada.protocol.ngp.common.mc.protocol.MessageDecoder;
import org.openscada.protocol.ngp.common.mc.protocol.MessageEncoder;
import org.openscada.protocol.ngp.common.mc.protocol.MessageProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BinaryProtocol implements MessageProtocol, MessageEncoder, MessageDecoder
{
    private final static Logger logger = LoggerFactory.getLogger ( BinaryProtocol.class );

    private final Map<Integer, BinaryMessageCodec> codeMap = new TreeMap<Integer, BinaryMessageCodec> ();

    private final Map<Class<?>, BinaryMessageCodec> classMap = new HashMap<Class<?>, BinaryMessageCodec> ();

    private final BinaryContext context;

    public BinaryProtocol ( final BinaryContext context, final Collection<BinaryMessageCodec> codecs )
    {
        this.context = context;

        // build up maps
        for ( final BinaryMessageCodec codec : codecs )
        {
            this.codeMap.put ( codec.getMessageCode (), codec );
            this.classMap.put ( codec.getMessageClass (), codec );
        }
    }

    @Override
    public MessageEncoder getEncoder ()
    {
        return this;
    }

    @Override
    public MessageDecoder getDecoder ()
    {
        return this;
    }

    @Override
    public Object decodeMessage ( final DataMessage message ) throws Exception
    {
        final IoBuffer data = message.getData ();

        // we only peek
        final int messageCode = data.getInt ( 0 );

        logger.trace ( "Decoding message: {}", messageCode );

        final BinaryMessageCodec codec = this.codeMap.get ( messageCode );
        if ( codec == null )
        {
            throw new IllegalStateException ( String.format ( "Unable to decode unknown message code: 0x%08x", messageCode ) );
        }

        return codec.decodeMessage ( this.context, data );
    }

    @Override
    public DataMessage encodeMessage ( final Object message ) throws Exception
    {
        if ( message == null )
        {
            throw new NullPointerException ( "Unable to encode null messages" );
        }

        final BinaryMessageCodec codec = this.classMap.get ( message.getClass () );
        if ( codec == null )
        {
            throw new IllegalArgumentException ( String.format ( "Message class is unknown. Unable to encode: %s", message.getClass () ) );
        }

        return new DataMessage ( codec.encodeMessage ( this.context, message ) );
    }

}
