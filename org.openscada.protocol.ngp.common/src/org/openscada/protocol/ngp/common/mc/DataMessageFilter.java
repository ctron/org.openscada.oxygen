/*
 * This file is part of the openSCADA project
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.protocol.ngp.common.mc;

import org.apache.mina.core.filterchain.IoFilterAdapter;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.write.WriteRequest;
import org.apache.mina.core.write.WriteRequestWrapper;
import org.openscada.protocol.ngp.common.mc.message.DataMessage;
import org.openscada.protocol.ngp.common.mc.protocol.MessageProtocol;

public class DataMessageFilter extends IoFilterAdapter
{

    private final MessageProtocol messageProtocol;

    public DataMessageFilter ( final MessageProtocol messageProtocol )
    {
        if ( messageProtocol == null )
        {
            throw new IllegalArgumentException ( "'messageProtocol' must not be null" );
        }

        this.messageProtocol = messageProtocol;
    }

    @Override
    public void filterWrite ( final NextFilter nextFilter, final IoSession session, final WriteRequest writeRequest ) throws Exception
    {
        final Object message = this.messageProtocol.getEncoder ().encodeMessage ( writeRequest.getMessage () );

        nextFilter.filterWrite ( session, new WriteRequestWrapper ( writeRequest ) {
            @Override
            public Object getMessage ()
            {
                return message;
            }
        } );
    }

    @Override
    public void messageReceived ( final NextFilter nextFilter, final IoSession session, final Object message ) throws Exception
    {
        if ( ! ( message instanceof DataMessage ) )
        {
            throw new IllegalStateException ( String.format ( "Can only handle messages of type %s. This message is: %s", DataMessage.class, message.getClass () ) );
        }

        nextFilter.messageReceived ( session, this.messageProtocol.getDecoder ().decodeMessage ( (DataMessage)message ) );
    }
}
