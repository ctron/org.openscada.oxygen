/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.protocol.ngp.common;

import org.apache.mina.core.filterchain.IoFilterChain;
import org.apache.mina.core.filterchain.IoFilterChainBuilder;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.logging.LoggingFilter;
import org.openscada.protocol.ngp.common.mc.MessageChannelCodecFilter;
import org.openscada.protocol.ngp.common.mc.MessageChannelFilter;
import org.openscada.protocol.ngp.common.mc.frame.FrameDecoder;
import org.openscada.protocol.ngp.common.mc.frame.FrameEncoder;

public class FilterChainBuilder implements IoFilterChainBuilder
{
    private final boolean clientMode;

    private String loggerName;

    public FilterChainBuilder ( final boolean clientMode )
    {
        this.clientMode = clientMode;
    }

    public void setLoggerName ( final String loggerName )
    {
        this.loggerName = loggerName;
    }

    public String getLoggerName ()
    {
        return this.loggerName;
    }

    @Override
    public void buildFilterChain ( final IoFilterChain chain )
    {
        if ( this.loggerName != null && Boolean.getBoolean ( "org.openscada.protocol.ngp.common.logger.raw" ) )
        {
            chain.addFirst ( "logger.raw", new LoggingFilter ( this.loggerName ) );
        }

        if ( !Boolean.getBoolean ( "org.openscada.protocol.ngp.common.disableStats" ) )
        {
            chain.addFirst ( StatisticsFilter.DEFAULT_NAME, new StatisticsFilter () );
        }

        if ( this.loggerName != null && Boolean.getBoolean ( "org.openscada.protocol.ngp.common.logger" ) )
        {
            chain.addFirst ( "logger", new LoggingFilter ( this.loggerName ) );
        }

        chain.addLast ( "frameCodec", new ProtocolCodecFilter ( new FrameEncoder (), new FrameDecoder () ) );

        chain.addLast ( "messageChannelCodec", new MessageChannelCodecFilter () );
        chain.addLast ( "messageChannel", new MessageChannelFilter ( this.clientMode ) );
    }
}
