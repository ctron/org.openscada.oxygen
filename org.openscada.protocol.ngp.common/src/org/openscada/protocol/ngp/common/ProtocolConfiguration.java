/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.protocol.ngp.common;

import java.util.LinkedList;
import java.util.List;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.compression.CompressionFilter;
import org.apache.mina.filter.ssl.SslContextFactory;
import org.openscada.protocol.ngp.common.mc.handshake.Handshake;
import org.openscada.protocol.ngp.common.mc.protocol.ProtocolDescriptor;
import org.openscada.protocol.ngp.common.mc.protocol.serialize.ObjectSerializationProtocolDescriptor;

public class ProtocolConfiguration
{
    private static final ProtocolConfiguration DEFAULT_CONFIGURATION;

    static
    {
        DEFAULT_CONFIGURATION = new ProtocolConfiguration ( null );
    }

    private int streamCompressionLevel = CompressionFilter.COMPRESSION_NONE;

    private int handshakeTimeout = Integer.getInteger ( "org.openscada.protocol.ngp.common.handshakeTimeout", 10000 );

    private int pingFrequency = 3;

    private int timeout = 10000;

    private SslContextFactory sslContextFactory;

    private List<String> preferredProtocols = new LinkedList<String> ();

    private List<ProtocolDescriptor> protocols = new LinkedList<ProtocolDescriptor> ();

    private List<Handshake> handshakeHandlers = new LinkedList<Handshake> ();

    public ProtocolConfiguration ( final ClassLoader classLoader )
    {
        this.protocols.add ( new ObjectSerializationProtocolDescriptor ( null, classLoader ) );
    }

    public ProtocolConfiguration ()
    {
    }

    public List<Handshake> getHandshakeHandlers ()
    {
        return this.handshakeHandlers;
    }

    public void setHandshakeHandlers ( final List<Handshake> handshakeHandlers )
    {
        this.handshakeHandlers = handshakeHandlers;
    }

    public void addHandshakeHandler ( final Handshake handshake )
    {
        this.handshakeHandlers.add ( handshake );
    }

    public void setPreferredProtocols ( final List<String> preferredProtocols )
    {
        this.preferredProtocols = preferredProtocols;
    }

    public List<String> getPreferredProtocols ()
    {
        return this.preferredProtocols;
    }

    public void setSslContextFactory ( final SslContextFactory sslContextFactory )
    {
        this.sslContextFactory = sslContextFactory;
    }

    public SslContextFactory getSslContextFactory ()
    {
        return this.sslContextFactory;
    }

    public void setPingFrequency ( final int pingFrequency )
    {
        this.pingFrequency = pingFrequency;
    }

    public int getPingFrequency ()
    {
        return this.pingFrequency;
    }

    public void setTimeout ( final int timeout )
    {
        this.timeout = timeout;
    }

    public int getTimeout ()
    {
        return this.timeout;
    }

    public void setStreamCompressionLevel ( final int streamCompressionLevel )
    {
        this.streamCompressionLevel = streamCompressionLevel;
    }

    public int getStreamCompressionLevel ()
    {
        return this.streamCompressionLevel;
    }

    public void setHandshakeTimeout ( final int handshakeTimeout )
    {
        this.handshakeTimeout = handshakeTimeout;
    }

    public int getHandshakeTimeout ()
    {
        return this.handshakeTimeout;
    }

    public static ProtocolConfiguration fromSession ( final IoSession session )
    {
        final Object result = session.getAttribute ( "protocol.configuration" );
        if ( result != null )
        {
            return (ProtocolConfiguration)result;
        }

        return DEFAULT_CONFIGURATION;
    }

    public void assign ( final IoSession session )
    {
        session.setAttribute ( "protocol.configuration", this );
    }

    public List<ProtocolDescriptor> getProtocols ()
    {
        return this.protocols;
    }

    public void setProtocols ( final List<ProtocolDescriptor> protocols )
    {
        this.protocols = protocols;
    }
}
