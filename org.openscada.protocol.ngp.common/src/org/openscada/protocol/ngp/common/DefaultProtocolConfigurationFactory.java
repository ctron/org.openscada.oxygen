/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.protocol.ngp.common;

import java.util.Map;

import org.apache.mina.filter.compression.CompressionFilter;
import org.apache.mina.filter.ssl.SslContextFactory;
import org.openscada.core.ConnectionInformation;
import org.openscada.protocol.ngp.common.mc.protocol.ProtocolDescriptor;
import org.openscada.protocol.ngp.common.mc.protocol.serialize.ObjectSerializationProtocolDescriptor;

public class DefaultProtocolConfigurationFactory implements ProtocolConfigurationFactory
{
    protected final ConnectionInformation connectionInformation;

    public DefaultProtocolConfigurationFactory ( final ConnectionInformation connectionInformation )
    {
        this.connectionInformation = connectionInformation;
    }

    @Override
    public ProtocolConfiguration createConfiguration ( final boolean clientMode ) throws Exception
    {
        final ProtocolConfiguration configuration = new ProtocolConfiguration ();

        configuration.setStreamCompressionLevel ( getInteger ( "streamCompressionLevel", this.connectionInformation.getProperties (), CompressionFilter.COMPRESSION_MAX ) );
        configuration.setSslContextFactory ( makeSslContextFactory ( clientMode ) );

        configuration.setTimeout ( getInteger ( "timeout", this.connectionInformation.getProperties (), configuration.getTimeout () ) ); //$NON-NLS-1$
        configuration.setPingFrequency ( getInteger ( "pingFrequency", this.connectionInformation.getProperties (), configuration.getPingFrequency () ) ); //$NON-NLS-1$

        customizeConfiguration ( configuration, clientMode );

        return configuration;
    }

    private Integer getInteger ( final String key, final Map<String, String> properties, final Integer defaultValue )
    {
        final String value = properties.get ( key );
        if ( value == null )
        {
            return defaultValue;
        }

        try
        {
            return Integer.parseInt ( value );
        }
        catch ( final Exception e )
        {
            return defaultValue;
        }
    }

    protected void addJavaProtocol ( final String protocolVersion, final ProtocolConfiguration configuration, final ClassLoader classLoader )
    {
        addProtocol ( configuration, new ObjectSerializationProtocolDescriptor ( "java/" + protocolVersion, classLoader ) ); //$NON-NLS-1$
    }

    protected void addProtocol ( final ProtocolConfiguration configuration, final ProtocolDescriptor protocol )
    {
        configuration.getProtocols ().add ( protocol );
    }

    /**
     * Can be overridden in order to customize the configuration
     * 
     * @param configuration
     *            the configuration to customize
     */
    protected void customizeConfiguration ( final ProtocolConfiguration configuration, final boolean clientMode )
    {
    }

    protected SslContextFactory makeSslContextFactory ( final boolean clientMode ) throws Exception
    {
        return SslHelper.createDefaultSslFactory ( this.connectionInformation.getProperties (), clientMode );
    }
}
