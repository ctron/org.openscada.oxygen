/*
 * This file is part of the openSCADA project
 *
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.hd.data.message;

public class CreateQuery implements java.io.Serializable
{
	private static final long serialVersionUID = 1L;

	public CreateQuery ( final org.openscada.core.data.Request request, final long queryId, final String itemId, final boolean updateData, final org.openscada.hd.data.QueryParameters queryParameters )
	{
		this.request = request;
		this.queryId = queryId;
		this.itemId = itemId;
		this.updateData = updateData;
		this.queryParameters = queryParameters;
	}

	private final org.openscada.core.data.Request request;
	
	public org.openscada.core.data.Request getRequest ()
	{
		return this.request;
	}

	private final long queryId;
	
	public long getQueryId ()
	{
		return this.queryId;
	}

	private final String itemId;
	
	public String getItemId ()
	{
		return this.itemId;
	}

	private final boolean updateData;
	
	public boolean isUpdateData ()
	{
		return this.updateData;
	}

	private final org.openscada.hd.data.QueryParameters queryParameters;
	
	public org.openscada.hd.data.QueryParameters getQueryParameters ()
	{
		return this.queryParameters;
	}
	
	@Override
	public String toString ()
	{
		return "[CreateQuery - " + 
			"request: " + this.request
	 + ", " +		"queryId: " + this.queryId
	 + ", " +		"itemId: " + this.itemId
	 + ", " +		"updateData: " + this.updateData
	 + ", " +		"queryParameters: " + this.queryParameters
			+ "]";
	}
}
