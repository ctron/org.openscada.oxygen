/*
 * This file is part of the openSCADA project
 *
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.hd.data.message;

public class UpdateQueryData implements java.io.Serializable
{
	private static final long serialVersionUID = 1L;

	public UpdateQueryData ( final long queryId, final int index, final java.util.List<org.openscada.hd.data.ValueInformation> valueInformation, final java.util.List<org.openscada.hd.data.ValueEntry> values )
	{
		this.queryId = queryId;
		this.index = index;
		this.valueInformation = valueInformation;
		this.values = values;
	}

	private final long queryId;
	
	public long getQueryId ()
	{
		return this.queryId;
	}

	private final int index;
	
	public int getIndex ()
	{
		return this.index;
	}

	private final java.util.List<org.openscada.hd.data.ValueInformation> valueInformation;
	
	public java.util.List<org.openscada.hd.data.ValueInformation> getValueInformation ()
	{
		return this.valueInformation;
	}

	private final java.util.List<org.openscada.hd.data.ValueEntry> values;
	
	public java.util.List<org.openscada.hd.data.ValueEntry> getValues ()
	{
		return this.values;
	}
	
	@Override
	public String toString ()
	{
		return "[UpdateQueryData - " + 
			"queryId: " + this.queryId
	 + ", " +		"index: " + this.index
	 + ", " +		"valueInformation: " + this.valueInformation
	 + ", " +		"values: " + this.values
			+ "]";
	}
}
