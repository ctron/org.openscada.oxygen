/*
 * This file is part of the openSCADA project
 *
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.ca.protocol.ngp.codec.impl;

import org.apache.mina.core.buffer.IoBuffer;

import org.openscada.protocol.ngp.common.mc.protocol.osbp.BinaryContext;
import org.openscada.protocol.ngp.common.mc.protocol.osbp.BinaryMessageCodec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GetConfigurationResponse implements BinaryMessageCodec
{
	private final static Logger logger = LoggerFactory.getLogger ( GetConfigurationResponse.class );

	public static final int MESSAGE_CODE = 16390;

	@Override
    public int getMessageCode ()
    {
    	return MESSAGE_CODE;
    }

	@Override
    public Class<?> getMessageClass ()
    {
       	return org.openscada.ca.data.message.GetConfigurationResponse.class;
    }

	@Override
    public org.openscada.ca.data.message.GetConfigurationResponse decodeMessage ( final BinaryContext _context, final IoBuffer _data ) throws Exception
    {
		// message code
		{
			final int messageCode = _data.getInt ();
	
			if ( messageCode != MESSAGE_CODE )
				throw new IllegalStateException ( String.format ( "Expected messageCode %s but found %s", MESSAGE_CODE, messageCode ) );
		}

		final byte numberOfFields = _data.get ();

		// decode attributes
		
		org.openscada.core.data.Response response = null;
		org.openscada.ca.data.ConfigurationInformation configuration = null;
		
		logger.trace ( "Decoding {} fields", numberOfFields );
		
		for ( int i = 0; i < numberOfFields; i++ )
		{
		
			final byte fieldNumber = _data.get ();
			switch ( fieldNumber ) {
			    case 1:
			    	{
			    		response = org.openscada.core.protocol.ngp.codec.Structures.decodeResponse ( _context, _data, false );
			    	}
			    	break;
			    case 2:
			    	{
			    		configuration = org.openscada.ca.protocol.ngp.codec.Structures.decodeConfigurationInformation ( _context, _data, false );
			    	}
			    	break;
				default:
					logger.warn ( "Received unknown field number: {}", fieldNumber ); 
					break;
			}
		
		}

		// create object
		return new org.openscada.ca.data.message.GetConfigurationResponse (
				response
		, 		configuration
			);
    }

    @Override
    public IoBuffer encodeMessage ( final BinaryContext context, final Object objectMessage ) throws Exception
    {
		final org.openscada.ca.data.message.GetConfigurationResponse value = (org.openscada.ca.data.message.GetConfigurationResponse)objectMessage;

		final IoBuffer data = IoBuffer.allocate ( 64 );
		data.setAutoExpand ( true );

		// encode message base
		data.putInt ( MESSAGE_CODE );

        // number of fields 
		data.put ( ((byte)2) );

		// encode attributes
		org.openscada.core.protocol.ngp.codec.Structures.encodeResponse ( context, data, ((byte)1), value.getResponse () );
		org.openscada.ca.protocol.ngp.codec.Structures.encodeConfigurationInformation ( context, data, ((byte)2), value.getConfiguration () );
		


		data.flip ();
		return data;
    }

}
