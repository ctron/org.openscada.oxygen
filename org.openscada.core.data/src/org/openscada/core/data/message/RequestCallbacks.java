/*
 * This file is part of the openSCADA project
 *
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.core.data.message;

public class RequestCallbacks implements java.io.Serializable, org.openscada.core.data.RequestMessage
{
	private static final long serialVersionUID = 1L;

	public RequestCallbacks ( final org.openscada.core.data.Request request, final long callbackHandlerId, final java.util.List<org.openscada.core.data.CallbackRequest> callbacks, final Long timeoutMillis )
	{
		this.request = request;
		this.callbackHandlerId = callbackHandlerId;
		this.callbacks = callbacks;
		this.timeoutMillis = timeoutMillis;
	}

	private final org.openscada.core.data.Request request;
	
	public org.openscada.core.data.Request getRequest ()
	{
		return this.request;
	}

	private final long callbackHandlerId;
	
	public long getCallbackHandlerId ()
	{
		return this.callbackHandlerId;
	}

	private final java.util.List<org.openscada.core.data.CallbackRequest> callbacks;
	
	public java.util.List<org.openscada.core.data.CallbackRequest> getCallbacks ()
	{
		return this.callbacks;
	}

	private final Long timeoutMillis;
	
	public Long getTimeoutMillis ()
	{
		return this.timeoutMillis;
	}
	
	@Override
	public String toString ()
	{
		return "[RequestCallbacks - " + 
			"request: " + this.request
	 + ", " +		"callbackHandlerId: " + this.callbackHandlerId
	 + ", " +		"callbacks: " + this.callbacks
	 + ", " +		"timeoutMillis: " + this.timeoutMillis
			+ "]";
	}
}
