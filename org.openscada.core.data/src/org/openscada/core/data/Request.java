/*
 * This file is part of the openSCADA project
 *
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.core.data;

public class Request implements java.io.Serializable
{
	private static final long serialVersionUID = 1L;

	public Request ( final long requestId )
	{
		this.requestId = requestId;
	}

	private final long requestId;
	
	public long getRequestId ()
	{
		return this.requestId;
	}
	@Override
	public boolean equals ( Object obj )
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
	
		if (!(obj instanceof Request))
			return false;
		Request other = (Request) obj;
	
		if ( this.requestId != other.requestId)
				return false;
	
	
		return true;
	}
	
	@Override
	public int hashCode ()
	{
		final int prime = 31;
		int result = 1;
	
		result = prime * result + (int) (this.requestId ^ (this.requestId >>> 32));
	
		return result;
	}
	@Override
	public String toString ()
	{
		return "[Request - " + 
			"requestId: " + this.requestId
			+ "]";
	}
}
