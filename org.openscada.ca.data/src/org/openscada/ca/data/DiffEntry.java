/*
 * This file is part of the openSCADA project
 *
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.ca.data;

public class DiffEntry implements java.io.Serializable
{
	private static final long serialVersionUID = 1L;

	public DiffEntry ( final String factoryId, final String configurationId, final org.openscada.ca.data.Operation operation, final java.util.Map<String, String> oldData, final java.util.Map<String, String> addedOrUpdatedData, final java.util.Set<String> removedData )
	{
		this.factoryId = factoryId;
		this.configurationId = configurationId;
		this.operation = operation;
		this.oldData = oldData;
		this.addedOrUpdatedData = addedOrUpdatedData;
		this.removedData = removedData;
	}

	private final String factoryId;
	
	public String getFactoryId ()
	{
		return this.factoryId;
	}

	private final String configurationId;
	
	public String getConfigurationId ()
	{
		return this.configurationId;
	}

	private final org.openscada.ca.data.Operation operation;
	
	public org.openscada.ca.data.Operation getOperation ()
	{
		return this.operation;
	}

	private final transient java.util.Map<String, String> oldData;
	
	public java.util.Map<String, String> getOldData ()
	{
		return this.oldData;
	}

	private final java.util.Map<String, String> addedOrUpdatedData;
	
	public java.util.Map<String, String> getAddedOrUpdatedData ()
	{
		return this.addedOrUpdatedData;
	}

	private final java.util.Set<String> removedData;
	
	public java.util.Set<String> getRemovedData ()
	{
		return this.removedData;
	}
	
	@Override
	public String toString ()
	{
		return "[DiffEntry - " + 
			"factoryId: " + this.factoryId
	 + ", " +		"configurationId: " + this.configurationId
	 + ", " +		"operation: " + this.operation
	 + ", " +		"oldData: " + this.oldData
	 + ", " +		"addedOrUpdatedData: " + this.addedOrUpdatedData
	 + ", " +		"removedData: " + this.removedData
			+ "]";
	}
}
