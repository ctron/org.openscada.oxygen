/*
 * This file is part of the openSCADA project
 *
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.ca.data.message;

public class GetFactoriesResponse implements java.io.Serializable, org.openscada.core.data.ResponseMessage
{
	private static final long serialVersionUID = 1L;

	public GetFactoriesResponse ( final org.openscada.core.data.Response response, final java.util.List<org.openscada.ca.data.FactoryInformation> factories )
	{
		this.response = response;
		this.factories = factories;
	}

	private final org.openscada.core.data.Response response;
	
	public org.openscada.core.data.Response getResponse ()
	{
		return this.response;
	}

	private final java.util.List<org.openscada.ca.data.FactoryInformation> factories;
	
	public java.util.List<org.openscada.ca.data.FactoryInformation> getFactories ()
	{
		return this.factories;
	}
	
	@Override
	public String toString ()
	{
		return "[GetFactoriesResponse - " + 
			"response: " + this.response
	 + ", " +		"factories: " + this.factories
			+ "]";
	}
}
