/*
 * This file is part of the openSCADA project
 *
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.ca.data;

public class FactoryInformation implements java.io.Serializable
{
	private static final long serialVersionUID = 1L;

	public FactoryInformation ( final String id, final String description, final org.openscada.ca.data.FactoryState state, final java.util.List<org.openscada.ca.data.ConfigurationInformation> configurations )
	{
		this.id = id;
		this.description = description;
		this.state = state;
		this.configurations = configurations;
	}

	private final String id;
	
	public String getId ()
	{
		return this.id;
	}

	private final String description;
	
	public String getDescription ()
	{
		return this.description;
	}

	private final org.openscada.ca.data.FactoryState state;
	
	public org.openscada.ca.data.FactoryState getState ()
	{
		return this.state;
	}

	private final java.util.List<org.openscada.ca.data.ConfigurationInformation> configurations;
	
	public java.util.List<org.openscada.ca.data.ConfigurationInformation> getConfigurations ()
	{
		return this.configurations;
	}
	@Override
	public boolean equals ( Object obj )
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
	
		if (!(obj instanceof FactoryInformation))
			return false;
		FactoryInformation other = (FactoryInformation) obj;
	
		if ( this.id == null )
		{
			if ( other.id != null )
				return false;
		}
		else if ( !this.id.equals ( other.id ) )
			return false;
	
	
		return true;
	}
	
	@Override
	public int hashCode ()
	{
		final int prime = 31;
		int result = 1;
	
		result = prime * result + ( this.id == null ? 0 : this.id.hashCode () );
	
		return result;
	}
	@Override
	public String toString ()
	{
		return "[FactoryInformation - " + 
			"id: " + this.id
	 + ", " +		"description: " + this.description
	 + ", " +		"state: " + this.state
	 + ", " +		"configurations: " + this.configurations
			+ "]";
	}
}
