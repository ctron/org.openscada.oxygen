/*
 * This file is part of the openSCADA project
 *
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.ca.data;

public class ConfigurationInformation implements java.io.Serializable
{
	private static final long serialVersionUID = 1L;

	public ConfigurationInformation ( final String factoryId, final String id, final org.openscada.ca.data.ConfigurationState state, final java.util.Map<String, String> data, final String errorInformation )
	{
		this.factoryId = factoryId;
		this.id = id;
		this.state = state;
		this.data = data;
		this.errorInformation = errorInformation;
	}

	private final String factoryId;
	
	public String getFactoryId ()
	{
		return this.factoryId;
	}

	private final String id;
	
	public String getId ()
	{
		return this.id;
	}

	private final org.openscada.ca.data.ConfigurationState state;
	
	public org.openscada.ca.data.ConfigurationState getState ()
	{
		return this.state;
	}

	private final java.util.Map<String, String> data;
	
	public java.util.Map<String, String> getData ()
	{
		return this.data;
	}

	private final String errorInformation;
	
	public String getErrorInformation ()
	{
		return this.errorInformation;
	}
	@Override
	public boolean equals ( Object obj )
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
	
		if (!(obj instanceof ConfigurationInformation))
			return false;
		ConfigurationInformation other = (ConfigurationInformation) obj;
	
		if ( this.id == null )
		{
			if ( other.id != null )
				return false;
		}
		else if ( !this.id.equals ( other.id ) )
			return false;
	
	
		return true;
	}
	
	@Override
	public int hashCode ()
	{
		final int prime = 31;
		int result = 1;
	
		result = prime * result + ( this.id == null ? 0 : this.id.hashCode () );
	
		return result;
	}
	@Override
	public String toString ()
	{
		return "[ConfigurationInformation - " + 
			"factoryId: " + this.factoryId
	 + ", " +		"id: " + this.id
	 + ", " +		"state: " + this.state
	 + ", " +		"data: " + this.data
	 + ", " +		"errorInformation: " + this.errorInformation
			+ "]";
	}
}
