/*
 * This file is part of the openSCADA project
 *
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.ae.data;

public class MonitorStatusInformation implements java.io.Serializable
{
	private static final long serialVersionUID = 1L;

	public MonitorStatusInformation ( final String id, final org.openscada.ae.data.MonitorStatus status, final long statusTimestamp, final org.openscada.ae.data.Severity severity, final org.openscada.core.Variant value, final Long lastAknTimestamp, final String lastAknUser, final Long lastFailTimestamp, final org.openscada.core.Variant lastFailValue, final java.util.Map<String, org.openscada.core.Variant> attributes )
	{
		this.id = id;
		this.status = status;
		this.statusTimestamp = statusTimestamp;
		this.severity = severity;
		this.value = value;
		this.lastAknTimestamp = lastAknTimestamp;
		this.lastAknUser = lastAknUser;
		this.lastFailTimestamp = lastFailTimestamp;
		this.lastFailValue = lastFailValue;
		this.attributes = attributes;
	}

	private final String id;
	
	public String getId ()
	{
		return this.id;
	}

	private final org.openscada.ae.data.MonitorStatus status;
	
	public org.openscada.ae.data.MonitorStatus getStatus ()
	{
		return this.status;
	}

	private final long statusTimestamp;
	
	public long getStatusTimestamp ()
	{
		return this.statusTimestamp;
	}

	private final org.openscada.ae.data.Severity severity;
	
	public org.openscada.ae.data.Severity getSeverity ()
	{
		return this.severity;
	}

	private final org.openscada.core.Variant value;
	
	public org.openscada.core.Variant getValue ()
	{
		return this.value;
	}

	private final Long lastAknTimestamp;
	
	public Long getLastAknTimestamp ()
	{
		return this.lastAknTimestamp;
	}

	private final String lastAknUser;
	
	public String getLastAknUser ()
	{
		return this.lastAknUser;
	}

	private final Long lastFailTimestamp;
	
	public Long getLastFailTimestamp ()
	{
		return this.lastFailTimestamp;
	}

	/**
	 * Hold the value that triggered the last failure
	 */
	private final org.openscada.core.Variant lastFailValue;
	
	public org.openscada.core.Variant getLastFailValue ()
	{
		return this.lastFailValue;
	}

	private final java.util.Map<String, org.openscada.core.Variant> attributes;
	
	public java.util.Map<String, org.openscada.core.Variant> getAttributes ()
	{
		return this.attributes;
	}
	
	@Override
	public String toString ()
	{
		return "[MonitorStatusInformation - " + 
			"id: " + this.id
	 + ", " +		"status: " + this.status
	 + ", " +		"statusTimestamp: " + this.statusTimestamp
	 + ", " +		"severity: " + this.severity
	 + ", " +		"value: " + this.value
	 + ", " +		"lastAknTimestamp: " + this.lastAknTimestamp
	 + ", " +		"lastAknUser: " + this.lastAknUser
	 + ", " +		"lastFailTimestamp: " + this.lastFailTimestamp
	 + ", " +		"lastFailValue: " + this.lastFailValue
	 + ", " +		"attributes: " + this.attributes
			+ "]";
	}
}
