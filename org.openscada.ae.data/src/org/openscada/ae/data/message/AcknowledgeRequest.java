/*
 * This file is part of the openSCADA project
 *
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.ae.data.message;

public class AcknowledgeRequest implements java.io.Serializable, org.openscada.core.data.RequestMessage
{
	private static final long serialVersionUID = 1L;

	public AcknowledgeRequest ( final org.openscada.core.data.Request request, final String monitorId, final Long aknTimestamp, final org.openscada.core.data.OperationParameters operationParameters, final Long callbackHandlerId )
	{
		this.request = request;
		this.monitorId = monitorId;
		this.aknTimestamp = aknTimestamp;
		this.operationParameters = operationParameters;
		this.callbackHandlerId = callbackHandlerId;
	}

	private final org.openscada.core.data.Request request;
	
	public org.openscada.core.data.Request getRequest ()
	{
		return this.request;
	}

	private final String monitorId;
	
	public String getMonitorId ()
	{
		return this.monitorId;
	}

	private final Long aknTimestamp;
	
	public Long getAknTimestamp ()
	{
		return this.aknTimestamp;
	}

	private final org.openscada.core.data.OperationParameters operationParameters;
	
	public org.openscada.core.data.OperationParameters getOperationParameters ()
	{
		return this.operationParameters;
	}

	private final Long callbackHandlerId;
	
	public Long getCallbackHandlerId ()
	{
		return this.callbackHandlerId;
	}
	
	@Override
	public String toString ()
	{
		return "[AcknowledgeRequest - " + 
			"request: " + this.request
	 + ", " +		"monitorId: " + this.monitorId
	 + ", " +		"aknTimestamp: " + this.aknTimestamp
	 + ", " +		"operationParameters: " + this.operationParameters
	 + ", " +		"callbackHandlerId: " + this.callbackHandlerId
			+ "]";
	}
}
