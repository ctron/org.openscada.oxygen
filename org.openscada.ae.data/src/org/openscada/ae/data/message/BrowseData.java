/*
 * This file is part of the openSCADA project
 *
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.ae.data.message;

public class BrowseData implements java.io.Serializable
{
	private static final long serialVersionUID = 1L;

	public BrowseData ( final java.util.List<org.openscada.ae.data.BrowserEntry> addedOrUpdated, final java.util.Set<String> removed, final boolean full )
	{
		this.addedOrUpdated = addedOrUpdated;
		this.removed = removed;
		this.full = full;
	}

	private final java.util.List<org.openscada.ae.data.BrowserEntry> addedOrUpdated;
	
	public java.util.List<org.openscada.ae.data.BrowserEntry> getAddedOrUpdated ()
	{
		return this.addedOrUpdated;
	}

	private final java.util.Set<String> removed;
	
	public java.util.Set<String> getRemoved ()
	{
		return this.removed;
	}

	private final boolean full;
	
	public boolean isFull ()
	{
		return this.full;
	}
	
	@Override
	public String toString ()
	{
		return "[BrowseData - " + 
			"addedOrUpdated: " + this.addedOrUpdated
	 + ", " +		"removed: " + this.removed
	 + ", " +		"full: " + this.full
			+ "]";
	}
}
