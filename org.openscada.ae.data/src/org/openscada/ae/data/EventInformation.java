/*
 * This file is part of the openSCADA project
 *
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.ae.data;

public class EventInformation implements java.io.Serializable
{
	private static final long serialVersionUID = 1L;

	public EventInformation ( final String id, final long sourceTimestamp, final long entryTimestamp, final java.util.Map<String, org.openscada.core.Variant> attributes )
	{
		this.id = id;
		this.sourceTimestamp = sourceTimestamp;
		this.entryTimestamp = entryTimestamp;
		this.attributes = attributes;
	}

	private final String id;
	
	public String getId ()
	{
		return this.id;
	}

	private final long sourceTimestamp;
	
	public long getSourceTimestamp ()
	{
		return this.sourceTimestamp;
	}

	private final long entryTimestamp;
	
	public long getEntryTimestamp ()
	{
		return this.entryTimestamp;
	}

	private final java.util.Map<String, org.openscada.core.Variant> attributes;
	
	public java.util.Map<String, org.openscada.core.Variant> getAttributes ()
	{
		return this.attributes;
	}
	
	@Override
	public String toString ()
	{
		return "[EventInformation - " + 
			"id: " + this.id
	 + ", " +		"sourceTimestamp: " + this.sourceTimestamp
	 + ", " +		"entryTimestamp: " + this.entryTimestamp
	 + ", " +		"attributes: " + this.attributes
			+ "]";
	}
}
