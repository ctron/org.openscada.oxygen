/*
 * This file is part of the openSCADA project
 *
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.ae.data;

public class BrowserEntry implements java.io.Serializable
{
	private static final long serialVersionUID = 1L;

	public BrowserEntry ( final String id, final java.util.Set<org.openscada.ae.data.BrowserType> types, final java.util.Map<String, org.openscada.core.Variant> attributes )
	{
		this.id = id;
		this.types = types;
		this.attributes = attributes;
	}

	private final String id;
	
	public String getId ()
	{
		return this.id;
	}

	private final java.util.Set<org.openscada.ae.data.BrowserType> types;
	
	public java.util.Set<org.openscada.ae.data.BrowserType> getTypes ()
	{
		return this.types;
	}

	private final java.util.Map<String, org.openscada.core.Variant> attributes;
	
	public java.util.Map<String, org.openscada.core.Variant> getAttributes ()
	{
		return this.attributes;
	}
	
	@Override
	public String toString ()
	{
		return "[BrowserEntry - " + 
			"id: " + this.id
	 + ", " +		"types: " + this.types
	 + ", " +		"attributes: " + this.attributes
			+ "]";
	}
}
