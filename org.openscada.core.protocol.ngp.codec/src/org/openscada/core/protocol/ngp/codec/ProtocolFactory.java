/*
 * This file is part of the openSCADA project
 *
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.core.protocol.ngp.codec;

import java.util.Collection;
import java.util.LinkedList;


import org.openscada.protocol.ngp.common.mc.protocol.osbp.BinaryContext;
import org.openscada.protocol.ngp.common.mc.protocol.osbp.BinaryMessageCodec;
import org.openscada.protocol.ngp.common.mc.protocol.osbp.BinaryProtocolDescriptor;

import org.openscada.protocol.ngp.common.mc.protocol.osbp.DefaultBinaryContext;

public final class ProtocolFactory
{
	private ProtocolFactory ()
	{
	}

	public static final String VERSION = "core.1";

	private final static class ProtocolDescriptor extends BinaryProtocolDescriptor
	{
		public ProtocolDescriptor ()
        {
            super ( new DefaultBinaryContext () );
        }

		public ProtocolDescriptor ( final BinaryContext binaryContext )
        {
            super ( binaryContext );
        }

		@Override
        public String getProtocolId ()
        {
           	return this.binaryContext.getProtocolIdPart () + '/' + VERSION;
        }

        @Override
        protected Collection<BinaryMessageCodec> getCodecs ()
        {
            final Collection<BinaryMessageCodec> codecs = new LinkedList<BinaryMessageCodec> ();

			ProtocolFactory.fillCodecs ( codecs );

            return codecs;
        }
	}

	public static org.openscada.protocol.ngp.common.mc.protocol.ProtocolDescriptor createProtocolDescriptor ()
	{
		return new ProtocolDescriptor ();
	}

	public static org.openscada.protocol.ngp.common.mc.protocol.ProtocolDescriptor createProtocolDescriptor ( final BinaryContext binaryContext )
	{
		return new ProtocolDescriptor ( binaryContext );
	}

	public static void fillCodecs ( Collection<BinaryMessageCodec> codecs )
	{
		// local messages 
		codecs.add ( new org.openscada.core.protocol.ngp.codec.impl.CreateSession () );
		codecs.add ( new org.openscada.core.protocol.ngp.codec.impl.SessionAccepted () );
		codecs.add ( new org.openscada.core.protocol.ngp.codec.impl.SessionRejected () );
		codecs.add ( new org.openscada.core.protocol.ngp.codec.impl.SessionPrivilegesChanged () );
		codecs.add ( new org.openscada.core.protocol.ngp.codec.impl.RequestCallbacks () );
		codecs.add ( new org.openscada.core.protocol.ngp.codec.impl.RespondCallbacks () );

	}
}

