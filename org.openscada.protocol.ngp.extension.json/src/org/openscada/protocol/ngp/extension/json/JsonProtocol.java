/*
 * This file is part of the openSCADA project
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.protocol.ngp.extension.json;

import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;

import org.apache.mina.core.buffer.IoBuffer;
import org.openscada.protocol.ngp.common.mc.message.DataMessage;
import org.openscada.protocol.ngp.common.mc.protocol.MessageDecoder;
import org.openscada.protocol.ngp.common.mc.protocol.MessageEncoder;
import org.openscada.protocol.ngp.common.mc.protocol.MessageProtocol;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @deprecated For the moment this does not work since we don't know which
 *             message
 *             object we have. Still we keep it as a reference on how it could
 *             be done.
 */
@Deprecated
public class JsonProtocol implements MessageProtocol, MessageEncoder, MessageDecoder
{

    private static GsonBuilder DEFAULT_BUILDER;

    static
    {
        DEFAULT_BUILDER = new GsonBuilder ();
    }

    private final Charset charset;

    private final CharsetEncoder encoder;

    private final CharsetDecoder decoder;

    private final Gson gson;

    public JsonProtocol ()
    {
        this ( Charset.forName ( "UTF-8" ), DEFAULT_BUILDER );
    }

    public JsonProtocol ( final Charset charset, final GsonBuilder builder )
    {
        this.charset = charset;
        this.encoder = charset.newEncoder ();
        this.decoder = charset.newDecoder ();

        this.gson = builder.create ();
    }

    @Override
    public MessageEncoder getEncoder ()
    {
        return this;
    }

    @Override
    public MessageDecoder getDecoder ()
    {
        return this;
    }

    @Override
    public Object decodeMessage ( final DataMessage message ) throws Exception
    {
        final InputStreamReader reader = new InputStreamReader ( message.getData ().asInputStream (), this.decoder );

        // FIXME: fix the next line
        return this.gson.fromJson ( reader, Object.class );
    }

    @Override
    public DataMessage encodeMessage ( final Object message ) throws Exception
    {
        final IoBuffer data = IoBuffer.allocate ( 0 );
        data.setAutoExpand ( true );

        final OutputStreamWriter out = new OutputStreamWriter ( data.asOutputStream (), this.encoder );
        this.gson.toJson ( message, out );

        data.flip ();
        return new DataMessage ( data );
    }

}
