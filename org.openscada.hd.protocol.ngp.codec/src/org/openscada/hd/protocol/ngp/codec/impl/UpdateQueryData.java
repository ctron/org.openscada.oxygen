/*
 * This file is part of the openSCADA project
 *
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.hd.protocol.ngp.codec.impl;

import org.apache.mina.core.buffer.IoBuffer;

import org.openscada.protocol.ngp.common.mc.protocol.osbp.BinaryContext;
import org.openscada.protocol.ngp.common.mc.protocol.osbp.BinaryMessageCodec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdateQueryData implements BinaryMessageCodec
{
	private final static Logger logger = LoggerFactory.getLogger ( UpdateQueryData.class );

	public static final int MESSAGE_CODE = 12295;

	@Override
    public int getMessageCode ()
    {
    	return MESSAGE_CODE;
    }

	@Override
    public Class<?> getMessageClass ()
    {
       	return org.openscada.hd.data.message.UpdateQueryData.class;
    }

	@Override
    public org.openscada.hd.data.message.UpdateQueryData decodeMessage ( final BinaryContext _context, final IoBuffer _data ) throws Exception
    {
		// message code
		{
			final int messageCode = _data.getInt ();
	
			if ( messageCode != MESSAGE_CODE )
				throw new IllegalStateException ( String.format ( "Expected messageCode %s but found %s", MESSAGE_CODE, messageCode ) );
		}

		final byte numberOfFields = _data.get ();

		// decode attributes
		
		long queryId = 0L;
		int index = 0;
		java.util.List<org.openscada.hd.data.ValueInformation> valueInformation = null;
		java.util.List<org.openscada.hd.data.ValueEntry> values = null;
		
		logger.trace ( "Decoding {} fields", numberOfFields );
		
		for ( int i = 0; i < numberOfFields; i++ )
		{
		
			final byte fieldNumber = _data.get ();
			switch ( fieldNumber ) {
			    case 1:
			    	{
			    		queryId = _context.decodePrimitiveLong ( _data );
			    	}
			    	break;
			    case 2:
			    	{
			    		index = _context.decodePrimitiveInt ( _data );
			    	}
			    	break;
			    case 3:
			    	{
			    		valueInformation = org.openscada.hd.protocol.ngp.codec.Structures.decodeListValueInformation ( _context, _data, true );
			    	}
			    	break;
			    case 4:
			    	{
			    		values = org.openscada.hd.protocol.ngp.codec.Structures.decodeListValueEntry ( _context, _data, true );
			    	}
			    	break;
				default:
					logger.warn ( "Received unknown field number: {}", fieldNumber ); 
					break;
			}
		
		}

		// create object
		return new org.openscada.hd.data.message.UpdateQueryData (
				queryId
		, 		index
		, 		valueInformation
		, 		values
			);
    }

    @Override
    public IoBuffer encodeMessage ( final BinaryContext context, final Object objectMessage ) throws Exception
    {
		final org.openscada.hd.data.message.UpdateQueryData value = (org.openscada.hd.data.message.UpdateQueryData)objectMessage;

		final IoBuffer data = IoBuffer.allocate ( 64 );
		data.setAutoExpand ( true );

		// encode message base
		data.putInt ( MESSAGE_CODE );

        // number of fields 
		data.put ( ((byte)4) );

		// encode attributes
		context.encodePrimitiveLong ( data, ((byte)1), value.getQueryId () );
		context.encodePrimitiveInt ( data, ((byte)2), value.getIndex () );
		org.openscada.hd.protocol.ngp.codec.Structures.encodeCollectionValueInformation ( context, data, ((byte)3), value.getValueInformation () );
		org.openscada.hd.protocol.ngp.codec.Structures.encodeCollectionValueEntry ( context, data, ((byte)4), value.getValues () );
		


		data.flip ();
		return data;
    }

}
