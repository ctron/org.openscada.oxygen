/*
 * This file is part of the openSCADA project
 *
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.hd.protocol.ngp.codec.impl;

import org.apache.mina.core.buffer.IoBuffer;

import org.openscada.protocol.ngp.common.mc.protocol.osbp.BinaryContext;
import org.openscada.protocol.ngp.common.mc.protocol.osbp.BinaryMessageCodec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListUpdate implements BinaryMessageCodec
{
	private final static Logger logger = LoggerFactory.getLogger ( ListUpdate.class );

	public static final int MESSAGE_CODE = 12547;

	@Override
    public int getMessageCode ()
    {
    	return MESSAGE_CODE;
    }

	@Override
    public Class<?> getMessageClass ()
    {
       	return org.openscada.hd.data.message.ListUpdate.class;
    }

	@Override
    public org.openscada.hd.data.message.ListUpdate decodeMessage ( final BinaryContext _context, final IoBuffer _data ) throws Exception
    {
		// message code
		{
			final int messageCode = _data.getInt ();
	
			if ( messageCode != MESSAGE_CODE )
				throw new IllegalStateException ( String.format ( "Expected messageCode %s but found %s", MESSAGE_CODE, messageCode ) );
		}

		final byte numberOfFields = _data.get ();

		// decode attributes
		
		java.util.Set<org.openscada.hd.data.HistoricalItemInformation> addedOrModified = null;
		java.util.Set<String> removed = null;
		boolean fullUpdate = false;
		
		logger.trace ( "Decoding {} fields", numberOfFields );
		
		for ( int i = 0; i < numberOfFields; i++ )
		{
		
			final byte fieldNumber = _data.get ();
			switch ( fieldNumber ) {
			    case 1:
			    	{
			    		addedOrModified = org.openscada.hd.protocol.ngp.codec.Structures.decodeSetHistoricalItemInformation ( _context, _data, true );
			    	}
			    	break;
			    case 2:
			    	{
			    		removed = _context.decodeStringSet ( _data );
			    	}
			    	break;
			    case 3:
			    	{
			    		fullUpdate = _context.decodePrimitiveBoolean ( _data );
			    	}
			    	break;
				default:
					logger.warn ( "Received unknown field number: {}", fieldNumber ); 
					break;
			}
		
		}

		// create object
		return new org.openscada.hd.data.message.ListUpdate (
				addedOrModified
		, 		removed
		, 		fullUpdate
			);
    }

    @Override
    public IoBuffer encodeMessage ( final BinaryContext context, final Object objectMessage ) throws Exception
    {
		final org.openscada.hd.data.message.ListUpdate value = (org.openscada.hd.data.message.ListUpdate)objectMessage;

		final IoBuffer data = IoBuffer.allocate ( 64 );
		data.setAutoExpand ( true );

		// encode message base
		data.putInt ( MESSAGE_CODE );

        // number of fields 
		data.put ( ((byte)3) );

		// encode attributes
		org.openscada.hd.protocol.ngp.codec.Structures.encodeCollectionHistoricalItemInformation ( context, data, ((byte)1), value.getAddedOrModified () );
		context.encodeStringCollection ( data, ((byte)2), value.getRemoved () );
		context.encodePrimitiveBoolean ( data, ((byte)3), value.isFullUpdate () );
		


		data.flip ();
		return data;
    }

}
