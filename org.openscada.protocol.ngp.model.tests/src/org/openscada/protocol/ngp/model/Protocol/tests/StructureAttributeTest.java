/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.protocol.ngp.model.Protocol.tests;

import junit.textui.TestRunner;

import org.openscada.protocol.ngp.model.Protocol.ProtocolFactory;
import org.openscada.protocol.ngp.model.Protocol.StructureAttribute;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Structure Attribute</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class StructureAttributeTest extends AttributeTest
{

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static void main ( String[] args )
    {
        TestRunner.run ( StructureAttributeTest.class );
    }

    /**
     * Constructs a new Structure Attribute test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public StructureAttributeTest ( String name )
    {
        super ( name );
    }

    /**
     * Returns the fixture for this Structure Attribute test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected StructureAttribute getFixture ()
    {
        return (StructureAttribute)fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp () throws Exception
    {
        setFixture ( ProtocolFactory.eINSTANCE.createStructureAttribute () );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown () throws Exception
    {
        setFixture ( null );
    }

} //StructureAttributeTest
