/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.protocol.ngp.model.Protocol.tests;

import junit.framework.TestCase;

import org.openscada.protocol.ngp.model.Protocol.AttributeStructure;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Attribute Structure</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class AttributeStructureTest extends TestCase
{

    /**
     * The fixture for this Attribute Structure test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected AttributeStructure fixture = null;

    /**
     * Constructs a new Attribute Structure test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public AttributeStructureTest ( String name )
    {
        super ( name );
    }

    /**
     * Sets the fixture for this Attribute Structure test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void setFixture ( AttributeStructure fixture )
    {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Attribute Structure test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected AttributeStructure getFixture ()
    {
        return fixture;
    }

} //AttributeStructureTest
