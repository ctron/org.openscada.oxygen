/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.protocol.ngp.model.Protocol.tests;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>Protocol</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class ProtocolTests extends TestSuite
{

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static void main ( String[] args )
    {
        TestRunner.run ( suite () );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static Test suite ()
    {
        TestSuite suite = new ProtocolTests ( "Protocol Tests" );
        return suite;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public ProtocolTests ( String name )
    {
        super ( name );
    }

} //ProtocolTests
