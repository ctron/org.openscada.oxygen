package org.openscada.protocol.ngp.test.hd;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.openscada.hd.data.ValueEntry;
import org.openscada.hd.data.ValueInformation;
import org.openscada.hd.data.message.UpdateQueryData;

public class DataGenerator
{
    private final Set<String> valueTypes;

    private final int entries;

    public DataGenerator ( final Set<String> valueTypes, final int entries )
    {
        this.valueTypes = valueTypes;
        this.entries = entries;
    }

    public UpdateQueryData createData ()
    {
        return new UpdateQueryData ( 0, 0, makeValueInformation (), makeValues () );
    }

    private List<ValueEntry> makeValues ()
    {
        final List<ValueEntry> result = new LinkedList<ValueEntry> ();

        for ( final String valueType : this.valueTypes )
        {
            result.add ( new ValueEntry ( valueType, makePlainValues () ) );
        }

        return result;
    }

    private List<Double> makePlainValues ()
    {
        final ArrayList<Double> result = new ArrayList<Double> ( this.entries );

        for ( int i = 0; i < this.entries; i++ )
        {
            result.add ( Double.valueOf ( i ) );
        }

        return result;
    }

    private List<ValueInformation> makeValueInformation ()
    {
        final List<ValueInformation> result = new ArrayList<ValueInformation> ( this.entries );

        for ( int i = 0; i < this.entries; i++ )
        {
            result.add ( new ValueInformation ( 1.0, 0.0, System.currentTimeMillis (), System.currentTimeMillis (), 10 ) );
        }

        return result;
    }
}
