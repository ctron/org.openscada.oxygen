/*
 * This file is part of the openSCADA project
 *
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.da.protocol.ngp.codec.impl;

import org.apache.mina.core.buffer.IoBuffer;

import org.openscada.protocol.ngp.common.mc.protocol.osbp.BinaryContext;
import org.openscada.protocol.ngp.common.mc.protocol.osbp.BinaryMessageCodec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StartWriteAttributes implements BinaryMessageCodec
{
	private final static Logger logger = LoggerFactory.getLogger ( StartWriteAttributes.class );

	public static final int MESSAGE_CODE = 4369;

	@Override
    public int getMessageCode ()
    {
    	return MESSAGE_CODE;
    }

	@Override
    public Class<?> getMessageClass ()
    {
       	return org.openscada.da.data.message.StartWriteAttributes.class;
    }

	@Override
    public org.openscada.da.data.message.StartWriteAttributes decodeMessage ( final BinaryContext _context, final IoBuffer _data ) throws Exception
    {
		// message code
		{
			final int messageCode = _data.getInt ();
	
			if ( messageCode != MESSAGE_CODE )
				throw new IllegalStateException ( String.format ( "Expected messageCode %s but found %s", MESSAGE_CODE, messageCode ) );
		}

		final byte numberOfFields = _data.get ();

		// decode attributes
		
		org.openscada.core.data.Request request = null;
		String itemId = null;
		java.util.Map<String, org.openscada.core.Variant> attributes = null;
		org.openscada.core.data.OperationParameters operationParameters = null;
		Long callbackHandlerId = null;
		
		logger.trace ( "Decoding {} fields", numberOfFields );
		
		for ( int i = 0; i < numberOfFields; i++ )
		{
		
			final byte fieldNumber = _data.get ();
			switch ( fieldNumber ) {
			    case 1:
			    	{
			    		request = org.openscada.core.protocol.ngp.codec.Structures.decodeRequest ( _context, _data, false );
			    	}
			    	break;
			    case 2:
			    	{
			    		itemId = _context.decodeString ( _data );
			    	}
			    	break;
			    case 3:
			    	{
			    		attributes = _context.decodeVariantMap ( _data );
			    	}
			    	break;
			    case 4:
			    	{
			    		operationParameters = org.openscada.core.protocol.ngp.codec.Structures.decodeOperationParameters ( _context, _data, true );
			    	}
			    	break;
			    case 5:
			    	{
			    		callbackHandlerId = _context.decodeLong ( _data );
			    	}
			    	break;
				default:
					logger.warn ( "Received unknown field number: {}", fieldNumber ); 
					break;
			}
		
		}

		// create object
		return new org.openscada.da.data.message.StartWriteAttributes (
				request
		, 		itemId
		, 		attributes
		, 		operationParameters
		, 		callbackHandlerId
			);
    }

    @Override
    public IoBuffer encodeMessage ( final BinaryContext context, final Object objectMessage ) throws Exception
    {
		final org.openscada.da.data.message.StartWriteAttributes value = (org.openscada.da.data.message.StartWriteAttributes)objectMessage;

		final IoBuffer data = IoBuffer.allocate ( 64 );
		data.setAutoExpand ( true );

		// encode message base
		data.putInt ( MESSAGE_CODE );

        // number of fields 
		data.put ( ((byte)5) );

		// encode attributes
		org.openscada.core.protocol.ngp.codec.Structures.encodeRequest ( context, data, ((byte)1), value.getRequest () );
		context.encodeString ( data, ((byte)2), value.getItemId () );
		context.encodeVariantMap ( data, ((byte)3), value.getAttributes () );
		org.openscada.core.protocol.ngp.codec.Structures.encodeOperationParameters ( context, data, ((byte)4), value.getOperationParameters () );
		context.encodeLong ( data, ((byte)5), value.getCallbackHandlerId () );
		


		data.flip ();
		return data;
    }

}
