/*
 * This file is part of the openSCADA project
 *
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.da.protocol.ngp.codec;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.mina.core.buffer.IoBuffer;

import org.openscada.protocol.ngp.common.mc.protocol.osbp.BinaryContext;

import org.openscada.protocol.ngp.common.utils.ArrayListAllocator;
import org.openscada.protocol.ngp.common.utils.CollectionAllocator;
import org.openscada.protocol.ngp.common.utils.HashSetAllocator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Structures
{

	private final static Logger logger = LoggerFactory.getLogger ( Structures.class );

	private Structures ()
	{
	}


	private static final ArrayListAllocator<org.openscada.da.data.AttributeWriteResultEntry> ALLOC_ATTRIBUTEWRITERESULTENTRY_LIST = new ArrayListAllocator<org.openscada.da.data.AttributeWriteResultEntry> ();

    private static final HashSetAllocator<org.openscada.da.data.AttributeWriteResultEntry> ALLOC_ATTRIBUTEWRITERESULTENTRY_SET = new HashSetAllocator<org.openscada.da.data.AttributeWriteResultEntry> ();

	protected static void inlineEncodeAttributeWriteResultEntry ( final BinaryContext context, final IoBuffer data, final org.openscada.da.data.AttributeWriteResultEntry value ) throws Exception
	{
        // number of fields
		data.put ( ((byte)2) );

        // encode attributes
		context.encodeString ( data, ((byte)1), value.getAttribute () );
		org.openscada.core.protocol.ngp.codec.Structures.encodeErrorInformation ( context, data, ((byte)2), value.getErrorInformation () );
		
	} 

	protected static org.openscada.da.data.AttributeWriteResultEntry inlindeDecodeAttributeWriteResultEntry ( final BinaryContext _context, final IoBuffer _data ) throws Exception
	{
		// number of fields
		final byte numberOfFields = _data.get ();

		// decode attributes
		
		String attribute = null;
		org.openscada.core.data.ErrorInformation errorInformation = null;
		
		logger.trace ( "Decoding {} fields", numberOfFields );
		
		for ( int i = 0; i < numberOfFields; i++ )
		{
		
			final byte fieldNumber = _data.get ();
			switch ( fieldNumber ) {
			    case 1:
			    	{
			    		attribute = _context.decodeString ( _data );
			    	}
			    	break;
			    case 2:
			    	{
			    		errorInformation = org.openscada.core.protocol.ngp.codec.Structures.decodeErrorInformation ( _context, _data, true );
			    	}
			    	break;
				default:
					logger.warn ( "Received unknown field number: {}", fieldNumber ); 
					break;
			}
		
		}

		// create object
		return new org.openscada.da.data.AttributeWriteResultEntry (
				attribute
		, 		errorInformation
			);
	}

	public static void encodeAttributeWriteResultEntry ( final BinaryContext context, final IoBuffer data, final byte fieldNumber, final org.openscada.da.data.AttributeWriteResultEntry value ) throws Exception
	{
		context.beginWriteStructure ( data, fieldNumber, value == null );

		if ( value != null )
		{
			inlineEncodeAttributeWriteResultEntry ( context, data, value );
		}
	}

	public static void encodeCollectionAttributeWriteResultEntry ( final BinaryContext context, final IoBuffer data, final byte fieldNumber, final Collection<org.openscada.da.data.AttributeWriteResultEntry> values ) throws Exception
	{
		context.beginWriteStructureList ( data, fieldNumber, values );

		if ( values == null )
		{
			return;
		}

		for ( org.openscada.da.data.AttributeWriteResultEntry entry : values )
		{
			inlineEncodeAttributeWriteResultEntry ( context, data, entry );
		}
	}

	public static org.openscada.da.data.AttributeWriteResultEntry decodeAttributeWriteResultEntry ( final BinaryContext context, final IoBuffer data, boolean allowNull ) throws Exception
	{
		final boolean isNull = context.beginReadStructure ( data, allowNull );

		if ( isNull )
		{
			return null;
		}
	
		return inlindeDecodeAttributeWriteResultEntry ( context, data );
	}

	protected static void fillAttributeWriteResultEntryCollection ( BinaryContext context, final IoBuffer data, final int items, final Collection<org.openscada.da.data.AttributeWriteResultEntry> values ) throws Exception
    {
        for ( int i = 0; i < items; i++ )
        {
            values.add ( inlindeDecodeAttributeWriteResultEntry ( context, data ) );
        }
    }

    protected static <T extends Collection<org.openscada.da.data.AttributeWriteResultEntry>> T decodeAttributeWriteResultEntryCollection ( final BinaryContext context, final IoBuffer data, final CollectionAllocator<org.openscada.da.data.AttributeWriteResultEntry, T> allactor, final boolean allowNull ) throws Exception
    {
		final Integer len = context.beginReadStructureList ( data, allowNull );

        if ( len == null )
        {
            return null;
        }
        else
        {
            final T result = allactor.allocate ( len );
            fillAttributeWriteResultEntryCollection ( context, data, len, result );
            return result;
        }
    }

    public static List<org.openscada.da.data.AttributeWriteResultEntry> decodeListAttributeWriteResultEntry ( final BinaryContext context, final IoBuffer data, final boolean allowNull ) throws Exception
    {
        return decodeAttributeWriteResultEntryCollection ( context, data, ALLOC_ATTRIBUTEWRITERESULTENTRY_LIST, allowNull );
    }

    public static Set<org.openscada.da.data.AttributeWriteResultEntry> decodeSetAttributeWriteResultEntry ( final BinaryContext context, final IoBuffer data, final boolean allowNull ) throws Exception
    {
        return decodeAttributeWriteResultEntryCollection ( context, data, ALLOC_ATTRIBUTEWRITERESULTENTRY_SET, allowNull );
    }


	private static final ArrayListAllocator<org.openscada.da.data.BrowserEntry> ALLOC_BROWSERENTRY_LIST = new ArrayListAllocator<org.openscada.da.data.BrowserEntry> ();

    private static final HashSetAllocator<org.openscada.da.data.BrowserEntry> ALLOC_BROWSERENTRY_SET = new HashSetAllocator<org.openscada.da.data.BrowserEntry> ();

	protected static void inlineEncodeBrowserEntry ( final BinaryContext context, final IoBuffer data, final org.openscada.da.data.BrowserEntry value ) throws Exception
	{
        // number of fields
		data.put ( ((byte)5) );

        // encode attributes
		context.encodeString ( data, ((byte)1), value.getName () );
		context.encodeEnum ( data, ((byte)2), value.getEntryType () );
		context.encodeString ( data, ((byte)3), value.getItemId () );
		context.encodeVariantMap ( data, ((byte)4), value.getAttributes () );
		context.encodeEnumSet ( data, ((byte)5), value.getIoDirection () );
		
	} 

	protected static org.openscada.da.data.BrowserEntry inlindeDecodeBrowserEntry ( final BinaryContext _context, final IoBuffer _data ) throws Exception
	{
		// number of fields
		final byte numberOfFields = _data.get ();

		// decode attributes
		
		String name = null;
		org.openscada.da.data.FolderEntryType entryType = null;
		String itemId = null;
		java.util.Map<String, org.openscada.core.Variant> attributes = null;
		java.util.Set<org.openscada.da.data.IODirection> ioDirection = null;
		
		logger.trace ( "Decoding {} fields", numberOfFields );
		
		for ( int i = 0; i < numberOfFields; i++ )
		{
		
			final byte fieldNumber = _data.get ();
			switch ( fieldNumber ) {
			    case 1:
			    	{
			    		name = _context.decodeString ( _data );
			    	}
			    	break;
			    case 2:
			    	{
			    		entryType = _context.decodeEnum ( _data, org.openscada.da.data.FolderEntryType.class );
			    	}
			    	break;
			    case 3:
			    	{
			    		itemId = _context.decodeString ( _data );
			    	}
			    	break;
			    case 4:
			    	{
			    		attributes = _context.decodeVariantMap ( _data );
			    	}
			    	break;
			    case 5:
			    	{
			    		ioDirection = _context.decodeEnumSet ( _data, org.openscada.da.data.IODirection.class );
			    	}
			    	break;
				default:
					logger.warn ( "Received unknown field number: {}", fieldNumber ); 
					break;
			}
		
		}

		// create object
		return new org.openscada.da.data.BrowserEntry (
				name
		, 		entryType
		, 		itemId
		, 		attributes
		, 		ioDirection
			);
	}

	public static void encodeBrowserEntry ( final BinaryContext context, final IoBuffer data, final byte fieldNumber, final org.openscada.da.data.BrowserEntry value ) throws Exception
	{
		context.beginWriteStructure ( data, fieldNumber, value == null );

		if ( value != null )
		{
			inlineEncodeBrowserEntry ( context, data, value );
		}
	}

	public static void encodeCollectionBrowserEntry ( final BinaryContext context, final IoBuffer data, final byte fieldNumber, final Collection<org.openscada.da.data.BrowserEntry> values ) throws Exception
	{
		context.beginWriteStructureList ( data, fieldNumber, values );

		if ( values == null )
		{
			return;
		}

		for ( org.openscada.da.data.BrowserEntry entry : values )
		{
			inlineEncodeBrowserEntry ( context, data, entry );
		}
	}

	public static org.openscada.da.data.BrowserEntry decodeBrowserEntry ( final BinaryContext context, final IoBuffer data, boolean allowNull ) throws Exception
	{
		final boolean isNull = context.beginReadStructure ( data, allowNull );

		if ( isNull )
		{
			return null;
		}
	
		return inlindeDecodeBrowserEntry ( context, data );
	}

	protected static void fillBrowserEntryCollection ( BinaryContext context, final IoBuffer data, final int items, final Collection<org.openscada.da.data.BrowserEntry> values ) throws Exception
    {
        for ( int i = 0; i < items; i++ )
        {
            values.add ( inlindeDecodeBrowserEntry ( context, data ) );
        }
    }

    protected static <T extends Collection<org.openscada.da.data.BrowserEntry>> T decodeBrowserEntryCollection ( final BinaryContext context, final IoBuffer data, final CollectionAllocator<org.openscada.da.data.BrowserEntry, T> allactor, final boolean allowNull ) throws Exception
    {
		final Integer len = context.beginReadStructureList ( data, allowNull );

        if ( len == null )
        {
            return null;
        }
        else
        {
            final T result = allactor.allocate ( len );
            fillBrowserEntryCollection ( context, data, len, result );
            return result;
        }
    }

    public static List<org.openscada.da.data.BrowserEntry> decodeListBrowserEntry ( final BinaryContext context, final IoBuffer data, final boolean allowNull ) throws Exception
    {
        return decodeBrowserEntryCollection ( context, data, ALLOC_BROWSERENTRY_LIST, allowNull );
    }

    public static Set<org.openscada.da.data.BrowserEntry> decodeSetBrowserEntry ( final BinaryContext context, final IoBuffer data, final boolean allowNull ) throws Exception
    {
        return decodeBrowserEntryCollection ( context, data, ALLOC_BROWSERENTRY_SET, allowNull );
    }

}
