/*
 * This file is part of the openSCADA project
 *
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.da.protocol.ngp.codec.impl;

import org.apache.mina.core.buffer.IoBuffer;

import org.openscada.protocol.ngp.common.mc.protocol.osbp.BinaryContext;
import org.openscada.protocol.ngp.common.mc.protocol.osbp.BinaryMessageCodec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ItemStateUpdate implements BinaryMessageCodec
{
	private final static Logger logger = LoggerFactory.getLogger ( ItemStateUpdate.class );

	public static final int MESSAGE_CODE = 4100;

	@Override
    public int getMessageCode ()
    {
    	return MESSAGE_CODE;
    }

	@Override
    public Class<?> getMessageClass ()
    {
       	return org.openscada.da.data.message.ItemStateUpdate.class;
    }

	@Override
    public org.openscada.da.data.message.ItemStateUpdate decodeMessage ( final BinaryContext _context, final IoBuffer _data ) throws Exception
    {
		// message code
		{
			final int messageCode = _data.getInt ();
	
			if ( messageCode != MESSAGE_CODE )
				throw new IllegalStateException ( String.format ( "Expected messageCode %s but found %s", MESSAGE_CODE, messageCode ) );
		}

		final byte numberOfFields = _data.get ();

		// decode attributes
		
		String itemId = null;
		org.openscada.core.data.SubscriptionState subscriptionState = null;
		org.openscada.core.data.ErrorInformation errorInformation = null;
		
		logger.trace ( "Decoding {} fields", numberOfFields );
		
		for ( int i = 0; i < numberOfFields; i++ )
		{
		
			final byte fieldNumber = _data.get ();
			switch ( fieldNumber ) {
			    case 1:
			    	{
			    		itemId = _context.decodeString ( _data );
			    	}
			    	break;
			    case 2:
			    	{
			    		subscriptionState = _context.decodeEnum ( _data, org.openscada.core.data.SubscriptionState.class );
			    	}
			    	break;
			    case 3:
			    	{
			    		errorInformation = org.openscada.core.protocol.ngp.codec.Structures.decodeErrorInformation ( _context, _data, true );
			    	}
			    	break;
				default:
					logger.warn ( "Received unknown field number: {}", fieldNumber ); 
					break;
			}
		
		}

		// create object
		return new org.openscada.da.data.message.ItemStateUpdate (
				itemId
		, 		subscriptionState
		, 		errorInformation
			);
    }

    @Override
    public IoBuffer encodeMessage ( final BinaryContext context, final Object objectMessage ) throws Exception
    {
		final org.openscada.da.data.message.ItemStateUpdate value = (org.openscada.da.data.message.ItemStateUpdate)objectMessage;

		final IoBuffer data = IoBuffer.allocate ( 64 );
		data.setAutoExpand ( true );

		// encode message base
		data.putInt ( MESSAGE_CODE );

        // number of fields 
		data.put ( ((byte)3) );

		// encode attributes
		context.encodeString ( data, ((byte)1), value.getItemId () );
		context.encodeEnum ( data, ((byte)2), value.getSubscriptionState () );
		org.openscada.core.protocol.ngp.codec.Structures.encodeErrorInformation ( context, data, ((byte)3), value.getErrorInformation () );
		


		data.flip ();
		return data;
    }

}
