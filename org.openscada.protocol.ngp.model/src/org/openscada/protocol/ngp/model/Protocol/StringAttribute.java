/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.protocol.ngp.model.Protocol;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>String Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.openscada.protocol.ngp.model.Protocol.ProtocolPackage#getStringAttribute()
 * @model
 * @generated
 */
public interface StringAttribute extends Attribute
{
} // StringAttribute
