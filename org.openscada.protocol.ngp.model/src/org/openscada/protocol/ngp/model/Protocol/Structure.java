/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.protocol.ngp.model.Protocol;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Structure</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openscada.protocol.ngp.model.Protocol.Structure#getProtocol <em>Protocol</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openscada.protocol.ngp.model.Protocol.ProtocolPackage#getStructure()
 * @model
 * @generated
 */
public interface Structure extends AttributeStructure
{

    /**
     * Returns the value of the '<em><b>Protocol</b></em>' container reference.
     * It is bidirectional and its opposite is '{@link org.openscada.protocol.ngp.model.Protocol.Protocol#getStructures <em>Structures</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Protocol</em>' container reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Protocol</em>' container reference.
     * @see #setProtocol(Protocol)
     * @see org.openscada.protocol.ngp.model.Protocol.ProtocolPackage#getStructure_Protocol()
     * @see org.openscada.protocol.ngp.model.Protocol.Protocol#getStructures
     * @model opposite="structures" required="true" transient="false"
     * @generated
     */
    Protocol getProtocol ();

    /**
     * Sets the value of the '{@link org.openscada.protocol.ngp.model.Protocol.Structure#getProtocol <em>Protocol</em>}' container reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Protocol</em>' container reference.
     * @see #getProtocol()
     * @generated
     */
    void setProtocol ( Protocol value );
} // Structure
