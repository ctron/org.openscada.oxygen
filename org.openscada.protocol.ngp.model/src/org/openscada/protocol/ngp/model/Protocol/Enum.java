/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.protocol.ngp.model.Protocol;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enum</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openscada.protocol.ngp.model.Protocol.Enum#getName <em>Name</em>}</li>
 *   <li>{@link org.openscada.protocol.ngp.model.Protocol.Enum#getLiterals <em>Literals</em>}</li>
 *   <li>{@link org.openscada.protocol.ngp.model.Protocol.Enum#getDescription <em>Description</em>}</li>
 *   <li>{@link org.openscada.protocol.ngp.model.Protocol.Enum#getProtocol <em>Protocol</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openscada.protocol.ngp.model.Protocol.ProtocolPackage#getEnum()
 * @model
 * @generated
 */
public interface Enum extends EObject
{
    /**
     * Returns the value of the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Name</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Name</em>' attribute.
     * @see #setName(String)
     * @see org.openscada.protocol.ngp.model.Protocol.ProtocolPackage#getEnum_Name()
     * @model id="true" required="true"
     * @generated
     */
    String getName ();

    /**
     * Sets the value of the '{@link org.openscada.protocol.ngp.model.Protocol.Enum#getName <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Name</em>' attribute.
     * @see #getName()
     * @generated
     */
    void setName ( String value );

    /**
     * Returns the value of the '<em><b>Literals</b></em>' attribute list.
     * The list contents are of type {@link java.lang.String}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Literals</em>' attribute list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Literals</em>' attribute list.
     * @see org.openscada.protocol.ngp.model.Protocol.ProtocolPackage#getEnum_Literals()
     * @model
     * @generated
     */
    EList<String> getLiterals ();

    /**
     * Returns the value of the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Description</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Description</em>' attribute.
     * @see #setDescription(String)
     * @see org.openscada.protocol.ngp.model.Protocol.ProtocolPackage#getEnum_Description()
     * @model
     * @generated
     */
    String getDescription ();

    /**
     * Sets the value of the '{@link org.openscada.protocol.ngp.model.Protocol.Enum#getDescription <em>Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Description</em>' attribute.
     * @see #getDescription()
     * @generated
     */
    void setDescription ( String value );

    /**
     * Returns the value of the '<em><b>Protocol</b></em>' container reference.
     * It is bidirectional and its opposite is '{@link org.openscada.protocol.ngp.model.Protocol.Protocol#getEnums <em>Enums</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Protocol</em>' container reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Protocol</em>' container reference.
     * @see #setProtocol(Protocol)
     * @see org.openscada.protocol.ngp.model.Protocol.ProtocolPackage#getEnum_Protocol()
     * @see org.openscada.protocol.ngp.model.Protocol.Protocol#getEnums
     * @model opposite="enums" transient="false"
     * @generated
     */
    Protocol getProtocol ();

    /**
     * Sets the value of the '{@link org.openscada.protocol.ngp.model.Protocol.Enum#getProtocol <em>Protocol</em>}' container reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Protocol</em>' container reference.
     * @see #getProtocol()
     * @generated
     */
    void setProtocol ( Protocol value );

} // Enum
