/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.protocol.ngp.model.Protocol;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Float Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.openscada.protocol.ngp.model.Protocol.ProtocolPackage#getFloatAttribute()
 * @model
 * @generated
 */
public interface FloatAttribute extends Attribute
{
} // FloatAttribute
