/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.protocol.ngp.model.Protocol;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variant Map Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.openscada.protocol.ngp.model.Protocol.ProtocolPackage#getVariantMapAttribute()
 * @model
 * @generated
 */
public interface VariantMapAttribute extends Attribute
{
} // VariantMapAttribute
