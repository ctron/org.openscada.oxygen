/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.protocol.ngp.model.Protocol.impl;

import org.eclipse.emf.ecore.EClass;
import org.openscada.protocol.ngp.model.Protocol.ProtocolPackage;
import org.openscada.protocol.ngp.model.Protocol.VariantMapAttribute;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Variant Map Attribute</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class VariantMapAttributeImpl extends AttributeImpl implements VariantMapAttribute
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected VariantMapAttributeImpl ()
    {
        super ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return ProtocolPackage.Literals.VARIANT_MAP_ATTRIBUTE;
    }

} //VariantMapAttributeImpl
