/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.protocol.ngp.model.Protocol.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.openscada.protocol.ngp.model.Protocol.Attribute;
import org.openscada.protocol.ngp.model.Protocol.ProtocolPackage;
import org.openscada.protocol.ngp.model.Protocol.Type;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attribute</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.openscada.protocol.ngp.model.Protocol.impl.AttributeImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.openscada.protocol.ngp.model.Protocol.impl.AttributeImpl#getFieldNumber <em>Field Number</em>}</li>
 *   <li>{@link org.openscada.protocol.ngp.model.Protocol.impl.AttributeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.openscada.protocol.ngp.model.Protocol.impl.AttributeImpl#isEquality <em>Equality</em>}</li>
 *   <li>{@link org.openscada.protocol.ngp.model.Protocol.impl.AttributeImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.openscada.protocol.ngp.model.Protocol.impl.AttributeImpl#isTransient <em>Transient</em>}</li>
 *   <li>{@link org.openscada.protocol.ngp.model.Protocol.impl.AttributeImpl#isDeleted <em>Deleted</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class AttributeImpl extends EObjectImpl implements Attribute
{
    /**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
    protected static final String NAME_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
    protected String name = NAME_EDEFAULT;

    /**
     * The default value of the '{@link #getFieldNumber() <em>Field Number</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getFieldNumber()
     * @generated
     * @ordered
     */
    protected static final byte FIELD_NUMBER_EDEFAULT = 0x00;

    /**
     * The cached value of the '{@link #getFieldNumber() <em>Field Number</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getFieldNumber()
     * @generated
     * @ordered
     */
    protected byte fieldNumber = FIELD_NUMBER_EDEFAULT;

    /**
     * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getDescription()
     * @generated
     * @ordered
     */
    protected static final String DESCRIPTION_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getDescription()
     * @generated
     * @ordered
     */
    protected String description = DESCRIPTION_EDEFAULT;

    /**
     * The default value of the '{@link #isEquality() <em>Equality</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isEquality()
     * @generated
     * @ordered
     */
    protected static final boolean EQUALITY_EDEFAULT = false;

    /**
     * The cached value of the '{@link #isEquality() <em>Equality</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isEquality()
     * @generated
     * @ordered
     */
    protected boolean equality = EQUALITY_EDEFAULT;

    /**
     * The default value of the '{@link #getType() <em>Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getType()
     * @generated
     * @ordered
     */
    protected static final Type TYPE_EDEFAULT = Type.SCALAR;

    /**
     * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getType()
     * @generated
     * @ordered
     */
    protected Type type = TYPE_EDEFAULT;

    /**
     * The default value of the '{@link #isTransient() <em>Transient</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isTransient()
     * @generated
     * @ordered
     */
    protected static final boolean TRANSIENT_EDEFAULT = false;

    /**
     * The cached value of the '{@link #isTransient() <em>Transient</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isTransient()
     * @generated
     * @ordered
     */
    protected boolean transient_ = TRANSIENT_EDEFAULT;

    /**
     * The default value of the '{@link #isDeleted() <em>Deleted</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isDeleted()
     * @generated
     * @ordered
     */
    protected static final boolean DELETED_EDEFAULT = false;

    /**
     * The cached value of the '{@link #isDeleted() <em>Deleted</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isDeleted()
     * @generated
     * @ordered
     */
    protected boolean deleted = DELETED_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected AttributeImpl ()
    {
        super ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return ProtocolPackage.Literals.ATTRIBUTE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String getName ()
    {
        return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setName ( String newName )
    {
        String oldName = name;
        name = newName;
        if ( eNotificationRequired () )
            eNotify ( new ENotificationImpl ( this, Notification.SET, ProtocolPackage.ATTRIBUTE__NAME, oldName, name ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public byte getFieldNumber ()
    {
        return fieldNumber;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setFieldNumber ( byte newFieldNumber )
    {
        byte oldFieldNumber = fieldNumber;
        fieldNumber = newFieldNumber;
        if ( eNotificationRequired () )
            eNotify ( new ENotificationImpl ( this, Notification.SET, ProtocolPackage.ATTRIBUTE__FIELD_NUMBER, oldFieldNumber, fieldNumber ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String getDescription ()
    {
        return description;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setDescription ( String newDescription )
    {
        String oldDescription = description;
        description = newDescription;
        if ( eNotificationRequired () )
            eNotify ( new ENotificationImpl ( this, Notification.SET, ProtocolPackage.ATTRIBUTE__DESCRIPTION, oldDescription, description ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public boolean isEquality ()
    {
        return equality;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setEquality ( boolean newEquality )
    {
        boolean oldEquality = equality;
        equality = newEquality;
        if ( eNotificationRequired () )
            eNotify ( new ENotificationImpl ( this, Notification.SET, ProtocolPackage.ATTRIBUTE__EQUALITY, oldEquality, equality ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Type getType ()
    {
        return type;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setType ( Type newType )
    {
        Type oldType = type;
        type = newType == null ? TYPE_EDEFAULT : newType;
        if ( eNotificationRequired () )
            eNotify ( new ENotificationImpl ( this, Notification.SET, ProtocolPackage.ATTRIBUTE__TYPE, oldType, type ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public boolean isTransient ()
    {
        return transient_;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setTransient ( boolean newTransient )
    {
        boolean oldTransient = transient_;
        transient_ = newTransient;
        if ( eNotificationRequired () )
            eNotify ( new ENotificationImpl ( this, Notification.SET, ProtocolPackage.ATTRIBUTE__TRANSIENT, oldTransient, transient_ ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public boolean isDeleted ()
    {
        return deleted;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setDeleted ( boolean newDeleted )
    {
        boolean oldDeleted = deleted;
        deleted = newDeleted;
        if ( eNotificationRequired () )
            eNotify ( new ENotificationImpl ( this, Notification.SET, ProtocolPackage.ATTRIBUTE__DELETED, oldDeleted, deleted ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet ( int featureID, boolean resolve, boolean coreType )
    {
        switch ( featureID )
        {
            case ProtocolPackage.ATTRIBUTE__NAME:
                return getName ();
            case ProtocolPackage.ATTRIBUTE__FIELD_NUMBER:
                return getFieldNumber ();
            case ProtocolPackage.ATTRIBUTE__DESCRIPTION:
                return getDescription ();
            case ProtocolPackage.ATTRIBUTE__EQUALITY:
                return isEquality ();
            case ProtocolPackage.ATTRIBUTE__TYPE:
                return getType ();
            case ProtocolPackage.ATTRIBUTE__TRANSIENT:
                return isTransient ();
            case ProtocolPackage.ATTRIBUTE__DELETED:
                return isDeleted ();
        }
        return super.eGet ( featureID, resolve, coreType );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eSet ( int featureID, Object newValue )
    {
        switch ( featureID )
        {
            case ProtocolPackage.ATTRIBUTE__NAME:
                setName ( (String)newValue );
                return;
            case ProtocolPackage.ATTRIBUTE__FIELD_NUMBER:
                setFieldNumber ( (Byte)newValue );
                return;
            case ProtocolPackage.ATTRIBUTE__DESCRIPTION:
                setDescription ( (String)newValue );
                return;
            case ProtocolPackage.ATTRIBUTE__EQUALITY:
                setEquality ( (Boolean)newValue );
                return;
            case ProtocolPackage.ATTRIBUTE__TYPE:
                setType ( (Type)newValue );
                return;
            case ProtocolPackage.ATTRIBUTE__TRANSIENT:
                setTransient ( (Boolean)newValue );
                return;
            case ProtocolPackage.ATTRIBUTE__DELETED:
                setDeleted ( (Boolean)newValue );
                return;
        }
        super.eSet ( featureID, newValue );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset ( int featureID )
    {
        switch ( featureID )
        {
            case ProtocolPackage.ATTRIBUTE__NAME:
                setName ( NAME_EDEFAULT );
                return;
            case ProtocolPackage.ATTRIBUTE__FIELD_NUMBER:
                setFieldNumber ( FIELD_NUMBER_EDEFAULT );
                return;
            case ProtocolPackage.ATTRIBUTE__DESCRIPTION:
                setDescription ( DESCRIPTION_EDEFAULT );
                return;
            case ProtocolPackage.ATTRIBUTE__EQUALITY:
                setEquality ( EQUALITY_EDEFAULT );
                return;
            case ProtocolPackage.ATTRIBUTE__TYPE:
                setType ( TYPE_EDEFAULT );
                return;
            case ProtocolPackage.ATTRIBUTE__TRANSIENT:
                setTransient ( TRANSIENT_EDEFAULT );
                return;
            case ProtocolPackage.ATTRIBUTE__DELETED:
                setDeleted ( DELETED_EDEFAULT );
                return;
        }
        super.eUnset ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet ( int featureID )
    {
        switch ( featureID )
        {
            case ProtocolPackage.ATTRIBUTE__NAME:
                return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals ( name );
            case ProtocolPackage.ATTRIBUTE__FIELD_NUMBER:
                return fieldNumber != FIELD_NUMBER_EDEFAULT;
            case ProtocolPackage.ATTRIBUTE__DESCRIPTION:
                return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals ( description );
            case ProtocolPackage.ATTRIBUTE__EQUALITY:
                return equality != EQUALITY_EDEFAULT;
            case ProtocolPackage.ATTRIBUTE__TYPE:
                return type != TYPE_EDEFAULT;
            case ProtocolPackage.ATTRIBUTE__TRANSIENT:
                return transient_ != TRANSIENT_EDEFAULT;
            case ProtocolPackage.ATTRIBUTE__DELETED:
                return deleted != DELETED_EDEFAULT;
        }
        return super.eIsSet ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString ()
    {
        if ( eIsProxy () )
            return super.toString ();

        StringBuffer result = new StringBuffer ( super.toString () );
        result.append ( " (name: " );
        result.append ( name );
        result.append ( ", fieldNumber: " );
        result.append ( fieldNumber );
        result.append ( ", description: " );
        result.append ( description );
        result.append ( ", equality: " );
        result.append ( equality );
        result.append ( ", type: " );
        result.append ( type );
        result.append ( ", transient: " );
        result.append ( transient_ );
        result.append ( ", deleted: " );
        result.append ( deleted );
        result.append ( ')' );
        return result.toString ();
    }

} //AttributeImpl
