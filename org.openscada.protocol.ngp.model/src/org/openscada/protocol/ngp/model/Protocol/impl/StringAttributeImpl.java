/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.protocol.ngp.model.Protocol.impl;

import org.eclipse.emf.ecore.EClass;
import org.openscada.protocol.ngp.model.Protocol.ProtocolPackage;
import org.openscada.protocol.ngp.model.Protocol.StringAttribute;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>String Attribute</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class StringAttributeImpl extends AttributeImpl implements StringAttribute
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected StringAttributeImpl ()
    {
        super ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return ProtocolPackage.Literals.STRING_ATTRIBUTE;
    }

} //StringAttributeImpl
