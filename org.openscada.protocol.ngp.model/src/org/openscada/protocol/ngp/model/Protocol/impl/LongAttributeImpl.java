/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.protocol.ngp.model.Protocol.impl;

import org.eclipse.emf.ecore.EClass;
import org.openscada.protocol.ngp.model.Protocol.LongAttribute;
import org.openscada.protocol.ngp.model.Protocol.ProtocolPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Long Attribute</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class LongAttributeImpl extends AttributeImpl implements LongAttribute
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected LongAttributeImpl ()
    {
        super ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return ProtocolPackage.Literals.LONG_ATTRIBUTE;
    }

} //LongAttributeImpl
