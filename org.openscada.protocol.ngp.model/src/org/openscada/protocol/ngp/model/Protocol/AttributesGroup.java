/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.protocol.ngp.model.Protocol;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attributes Group</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.openscada.protocol.ngp.model.Protocol.ProtocolPackage#getAttributesGroup()
 * @model
 * @generated
 */
public interface AttributesGroup extends EObject
{
} // AttributesGroup
