/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.protocol.ngp.model.Protocol;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Properties Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.openscada.protocol.ngp.model.Protocol.ProtocolPackage#getPropertiesAttribute()
 * @model
 * @generated
 */
public interface PropertiesAttribute extends Attribute
{
} // PropertiesAttribute
