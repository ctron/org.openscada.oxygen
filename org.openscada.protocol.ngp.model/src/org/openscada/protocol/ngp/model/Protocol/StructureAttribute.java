/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.protocol.ngp.model.Protocol;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Structure Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openscada.protocol.ngp.model.Protocol.StructureAttribute#getStructure <em>Structure</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openscada.protocol.ngp.model.Protocol.ProtocolPackage#getStructureAttribute()
 * @model
 * @generated
 */
public interface StructureAttribute extends Attribute
{
    /**
     * Returns the value of the '<em><b>Structure</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Structure</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Structure</em>' reference.
     * @see #setStructure(Structure)
     * @see org.openscada.protocol.ngp.model.Protocol.ProtocolPackage#getStructureAttribute_Structure()
     * @model required="true"
     * @generated
     */
    Structure getStructure ();

    /**
     * Sets the value of the '{@link org.openscada.protocol.ngp.model.Protocol.StructureAttribute#getStructure <em>Structure</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Structure</em>' reference.
     * @see #getStructure()
     * @generated
     */
    void setStructure ( Structure value );

} // StructureAttribute
