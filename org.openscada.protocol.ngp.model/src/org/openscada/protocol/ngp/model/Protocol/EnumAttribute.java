/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.protocol.ngp.model.Protocol;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enum Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openscada.protocol.ngp.model.Protocol.EnumAttribute#getEnumType <em>Enum Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openscada.protocol.ngp.model.Protocol.ProtocolPackage#getEnumAttribute()
 * @model
 * @generated
 */
public interface EnumAttribute extends Attribute
{
    /**
     * Returns the value of the '<em><b>Enum Type</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Enum Type</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Enum Type</em>' reference.
     * @see #setEnumType(org.openscada.protocol.ngp.model.Protocol.Enum)
     * @see org.openscada.protocol.ngp.model.Protocol.ProtocolPackage#getEnumAttribute_EnumType()
     * @model required="true"
     * @generated
     */
    org.openscada.protocol.ngp.model.Protocol.Enum getEnumType ();

    /**
     * Sets the value of the '{@link org.openscada.protocol.ngp.model.Protocol.EnumAttribute#getEnumType <em>Enum Type</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Enum Type</em>' reference.
     * @see #getEnumType()
     * @generated
     */
    void setEnumType ( org.openscada.protocol.ngp.model.Protocol.Enum value );

} // EnumAttribute
