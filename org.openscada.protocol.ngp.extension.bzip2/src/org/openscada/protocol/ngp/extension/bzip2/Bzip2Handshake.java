/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.protocol.ngp.extension.bzip2;

import java.util.Map;

import org.openscada.protocol.ngp.common.mc.handshake.Handshake;
import org.openscada.protocol.ngp.common.mc.handshake.HandshakeContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Bzip2Handshake implements Handshake
{

    private final static Logger logger = LoggerFactory.getLogger ( Bzip2Handshake.class );

    @Override
    public void request ( final HandshakeContext context, final Map<String, String> helloProperties )
    {
        helloProperties.put ( "bzip2.enable", "true" );
    }

    @Override
    public void handshake ( final HandshakeContext context, final Map<String, String> helloProperties, final Map<String, String> acceptedProperties ) throws Exception
    {
        final String value = helloProperties.get ( "bzip2.enable" );
        if ( value == null )
        {
            return;
        }
        if ( Boolean.parseBoolean ( value ) )
        {
            logger.info ( "Agreed on bzip2 filter" );
            acceptedProperties.put ( "bzip2.enable", "true" );
        }
    }

    @Override
    public void apply ( final HandshakeContext context, final Map<String, String> acceptedProperties ) throws Exception
    {
        final String value = acceptedProperties.get ( "bzip2.enable" );
        if ( value == null )
        {
            return;
        }
        if ( Boolean.parseBoolean ( value ) )
        {
            logger.info ( "Injecting bzip2 filter" );
            context.getSession ().getFilterChain ().addAfter ( "frameCodec", "bzip2", new Bzip2FrameCompressionFilter () );
        }
    }

    @Override
    public void postApply ( final HandshakeContext context, final Map<String, String> acceptedProperties ) throws Exception
    {
    }

    @Override
    public void sessionStarted ( final HandshakeContext context, final Map<String, String> acceptedProperties ) throws Exception
    {
    }

}
