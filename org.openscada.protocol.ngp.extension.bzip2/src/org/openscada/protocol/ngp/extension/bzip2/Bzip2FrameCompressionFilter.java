/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.protocol.ngp.extension.bzip2;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.filterchain.IoFilterAdapter;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.write.WriteRequest;
import org.apache.mina.core.write.WriteRequestWrapper;
import org.itadaki.bzip2.BZip2InputStream;
import org.itadaki.bzip2.BZip2OutputStream;
import org.openscada.protocol.ngp.common.mc.frame.Frame;
import org.openscada.protocol.ngp.common.mc.frame.Frame.FrameType;

import com.google.common.io.ByteStreams;

public class Bzip2FrameCompressionFilter extends IoFilterAdapter
{
    private IoBuffer compress ( final IoBuffer data ) throws IOException
    {
        final ByteArrayInputStream input = new ByteArrayInputStream ( data.array () );
        final ByteArrayOutputStream output = new ByteArrayOutputStream ();
        final BZip2OutputStream bzip2 = new BZip2OutputStream ( output );

        ByteStreams.copy ( input, bzip2 );
        bzip2.close ();

        final IoBuffer result = IoBuffer.wrap ( output.toByteArray () );
        return result;
    }

    private IoBuffer decompress ( final IoBuffer data ) throws IOException
    {
        final ByteArrayInputStream input = new ByteArrayInputStream ( data.array () );
        final ByteArrayOutputStream output = new ByteArrayOutputStream ();
        final BZip2InputStream bzip2 = new BZip2InputStream ( input, false );

        ByteStreams.copy ( bzip2, output );
        bzip2.close ();
        output.close ();

        final IoBuffer result = IoBuffer.wrap ( output.toByteArray () );
        return result;
    }

    @Override
    public void filterWrite ( final NextFilter nextFilter, final IoSession session, final WriteRequest writeRequest ) throws Exception
    {
        final Object o = writeRequest.getMessage ();

        if ( o instanceof Frame && ( (Frame)o ).getType () == FrameType.MESSAGE )
        {
            if ( ! ( (Frame)o ).getData ().hasRemaining () )
            {
                nextFilter.filterWrite ( session, writeRequest );
            }
            else
            {
                final Frame compressedFrame = new Frame ( ( (Frame)o ).getType (), compress ( ( (Frame)o ).getData () ) );
                nextFilter.filterWrite ( session, new WriteRequestWrapper ( writeRequest ) {
                    @Override
                    public Object getMessage ()
                    {
                        return compressedFrame;
                    };
                } );
            }
        }
        else
        {
            super.filterWrite ( nextFilter, session, writeRequest );
        }
    }

    @Override
    public void messageReceived ( final NextFilter nextFilter, final IoSession session, final Object message ) throws Exception
    {
        if ( message instanceof Frame && ( (Frame)message ).getType () == FrameType.MESSAGE )
        {
            if ( ! ( (Frame)message ).getData ().hasRemaining () )
            {
                nextFilter.messageReceived ( session, message );
            }
            else
            {
                nextFilter.messageReceived ( session, new Frame ( ( (Frame)message ).getType (), decompress ( ( (Frame)message ).getData () ) ) );
            }
        }
        else
        {
            super.messageReceived ( nextFilter, session, message );
        }
    }
}
