/*
 * This file is part of the openSCADA project
 *
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.da.data;

public class BrowserEntry implements java.io.Serializable
{
	private static final long serialVersionUID = 1L;

	public BrowserEntry ( final String name, final org.openscada.da.data.FolderEntryType entryType, final String itemId, final java.util.Map<String, org.openscada.core.Variant> attributes, final java.util.Set<org.openscada.da.data.IODirection> ioDirection )
	{
		this.name = name;
		this.entryType = entryType;
		this.itemId = itemId;
		this.attributes = attributes;
		this.ioDirection = ioDirection;
	}

	private final String name;
	
	public String getName ()
	{
		return this.name;
	}

	private final org.openscada.da.data.FolderEntryType entryType;
	
	public org.openscada.da.data.FolderEntryType getEntryType ()
	{
		return this.entryType;
	}

	private final String itemId;
	
	public String getItemId ()
	{
		return this.itemId;
	}

	private final java.util.Map<String, org.openscada.core.Variant> attributes;
	
	public java.util.Map<String, org.openscada.core.Variant> getAttributes ()
	{
		return this.attributes;
	}

	private final java.util.Set<org.openscada.da.data.IODirection> ioDirection;
	
	public java.util.Set<org.openscada.da.data.IODirection> getIoDirection ()
	{
		return this.ioDirection;
	}
	
	@Override
	public String toString ()
	{
		return "[BrowserEntry - " + 
			"name: " + this.name
	 + ", " +		"entryType: " + this.entryType
	 + ", " +		"itemId: " + this.itemId
	 + ", " +		"attributes: " + this.attributes
	 + ", " +		"ioDirection: " + this.ioDirection
			+ "]";
	}
}
