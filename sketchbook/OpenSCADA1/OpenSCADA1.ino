
#include <SPI.h>         // needed for Arduino versions later than 0018
#include <Ethernet.h>
#include <EthernetUdp.h>         // UDP library from: bjoern@cs.stanford.edu 12/30/2008
#include <OpenSCADA.h>
#include <EEPROM.h>


#define T_KELVIN 0
#define T_CELSIUS 1
#define T_FAHRENHEIT 2


 // the media access control (ethernet hardware) address for the shield:
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };  
//the IP address for the shield:
byte ip[] = { 172, 20, 11, 105 };
// the router's gateway address:
byte gateway[] = { 172, 20, 10, 1 };
// the subnet:
byte subnet[] = { 255, 255, 0, 0 };

unsigned int localPort = 8888;      // local port to listen on

EthernetUDP udp;

OpenSCADA<7> scada ( udp );
OpenSCADAItem & temperature ( scada.getItem ( 0 ) );
OpenSCADAItem & rawValue ( scada.getItem ( 6 ) );
OpenSCADAItem & state ( scada.getItem ( 1 ) );
OpenSCADAItem & nextStateItem ( scada.getItem ( 5 ) );

OpenSCADAItem & stopState ( scada.getItem ( 2 ) );
OpenSCADAItem & minLevel ( scada.getItem ( 3 ) );
OpenSCADAItem & maxLevel ( scada.getItem ( 4 ) );

#define RED_PIN 6
#define YELLOW_PIN 5
#define GREEN_PIN 3

long loopCounter = 0;
short currentState = 0;
short nextState = 0;

float minValue = 20.0f;
float maxValue = 25.0f;

unsigned long now = 0;
unsigned long lastChange = 0;

boolean stopMode = false;

void setup() {
  
  initEEPROM ();
  
  // setup ethernet chip, deselect SD card
  pinMode ( 4, OUTPUT );
  digitalWrite ( 4, HIGH );
  
  pinMode ( RED_PIN, OUTPUT );
  pinMode ( YELLOW_PIN, OUTPUT );
  pinMode ( GREEN_PIN, OUTPUT );
  
  Serial.begin ( 9600 );
  Serial.println ( "========= RESET =========" );
  
  Serial.print ( "Max Packet Size: " );
  Serial.print ( OS_PACKET_BUFFER_SIZE );
  Serial.println ();
  
  // start the Ethernet and UDP:
  Ethernet.begin ( mac, ip, gateway, gateway, subnet );
  udp.begin ( localPort );
  
  // current temperature
  scada.setItem ( 0, "temperature", true, false );
  scada.setItem ( 6, "rawValue", true, true );
    
  // current state
  scada.setItem ( 1, "state", true, false );
    
  // stop condition
  scada.setItem ( 2, "stop", true, true );
  
  // temperature min/max setting
  scada.setItem ( 3, "min", true, true );
  scada.setItem ( 4, "max", true, true );
  
  scada.setItem ( 5, "nextState", true, true );
  

  
  minValue = readFloat ( 0 );
  maxValue = readFloat ( 1 );
  
  state.value ( 0 );
  nextStateItem.value ( 0 );
  minLevel.value ( minValue );
  maxLevel.value ( maxValue );
  stopState.value ( false );
}


float Temperature(int AnalogInputNumber, int unit,float B,float T0,float R0,float R_Balance)
{
  float R,T;

  R=1024.0f*R_Balance/float(analogRead(AnalogInputNumber))-R_Balance;
  T=1.0f/(1.0f/T0+(1.0f/B)*log(R/R0));

  switch(unit) {
    case T_CELSIUS:
      T-=273.15f;
    break;
    case T_FAHRENHEIT:
      T=9.0f*(T-273.15f)/5.0f+32.0f;
    break;
    default:
    break;
  };

  return T;
}

#define SWITCH_DELAY 1000

void initEEPROM ()
{
  uint8_t magic1 = EEPROM.read ( 0 );
  uint8_t magic2 = EEPROM.read ( 1 );
  if ( magic1 == 0x12 && magic2 == 0x02 )
  {
    return;
  }
  
  EEPROM.write ( 0, 0x12 );
  EEPROM.write ( 1, 0x02 );
  storeFloat ( 0, 20.0f );
  storeFloat ( 1, 25.0f );
}

void storeFloat ( int offset, float value )
{
  int pos = 2 + ( offset  * 4 );
  uint8_t * p = (uint8_t*)&value;
  EEPROM.write ( pos, p[0] );
  EEPROM.write ( pos + 1, p[1] );
  EEPROM.write ( pos + 2, p[2] );
  EEPROM.write ( pos + 3, p[3] );
}

float readFloat ( int offset )
{
  int pos = 2 + ( offset  * 4 );
  float f;
  uint8_t * p = (uint8_t*)&f;
  p[0] = EEPROM.read ( pos );
  p[1] = EEPROM.read ( pos + 1 );
  p[2] = EEPROM.read ( pos + 2 );
  p[3] = EEPROM.read ( pos + 3 );
  return f;
}

float oldTemp;

void loop() {
  
  scada.processPackets ();
  
  float newTemp = Temperature ( 0, T_CELSIUS, 3740.0f,298.15f,22000.0f,1000.0f );
  float temp = ( oldTemp + newTemp ) / 2.0f;
  oldTemp = temp;
  
  scada.setInt32Value ( 6, analogRead(0) );
  scada.setDoubleValue ( 0,  ((float)((int)( temp * 100.0 ))) / 100.0 );
  
  if ( nextStateItem.checkAndResetWrite () )
  {
    nextState = state.int32WriteRequest ();
  }
  if ( stopState.checkAndResetWrite () )
  {
    stopMode = stopState.booleanWriteRequest ();
    stopState.value ( stopMode );
  }
  if ( maxLevel.checkAndResetWrite () )
  {
    maxValue = maxLevel.doubleWriteRequest ( maxValue );
    maxLevel.value ( maxValue );
    storeFloat ( 1, maxValue );
  }
  if ( minLevel.checkAndResetWrite () )
  {
    minValue = minLevel.doubleWriteRequest ( minValue );
    minLevel.value ( minValue );
    storeFloat ( 0, minValue );
  }
  
  now = millis ();
  
  if ( temp < minValue || temp > maxValue )
  {
    if ( nextState != 1 )
    {
      nextState = 1;
      nextStateItem.value ( nextState );
    }
  }
  else
  {
    if ( nextState != 3 )
    {
      nextState = 3;
      nextStateItem.value ( nextState );
    }
  }
  
  advanceState ();
  triggerStateChange ();
  processState ();
  
  state.value ( currentState );
}

void triggerStateChange ()
{
  if ( stopMode )
    return;
  if ( currentState == nextState )
    return;
    
  if ( now - lastChange <= SWITCH_DELAY )
    return;
    
  switch ( nextState )
  {
    case 1:
      currentState = ( currentState == 0 ) ? 1 : 2;
      lastChange = now;
      break;
    case 3:
      currentState = 4;
      lastChange = now;
      break;
  }
}

void advanceState ()
{
  if ( stopMode )
  {
    currentState = 0;
    return;
  }
  
  switch ( currentState )
  {
    case 2: // yellow
      {
         if ( now - lastChange > SWITCH_DELAY )
         {
           lastChange = now;
           currentState = 1;
         }
      }
      break;
    case 4: // red and yellow
      {
        if ( now - lastChange > SWITCH_DELAY )
        {
          lastChange = now;
          currentState = 3;
        }
      }
      break;
  }
}

void setColors ( boolean red, boolean yellow, boolean green )
{
  digitalWrite ( RED_PIN, red );
  digitalWrite ( YELLOW_PIN, yellow );
  digitalWrite ( GREEN_PIN, green );
}

#define TOGGLE (( now >> 9 ) % 2 == 0)

/*
 * Handle traffic light state
 */
void processState ()
{ 
  switch ( currentState )
  {
   case 0:
     setColors ( false, TOGGLE, false );
     break;
   case 1: // red
     setColors ( true, false, false );
     break;
   case 2: // yellow
     setColors ( false, true, false );
     break;
   case 3: // green
     setColors ( false, false, true );
     break;
   case 4: // red and yellow
     setColors ( true, true, false );
     break;
   default:
     boolean toggle = TOGGLE;
     setColors ( toggle, toggle, toggle );
     break;
  }
}

