#ifndef _OPENSCADA_H_
#define _OPENSCADA_H_

#include <SPI.h>
#include <Ethernet.h>
#include <EthernetUdp.h>
#include <string.h>

#define OS_PACKET_BUFFER_SIZE 512

static uint8_t replyBuffer[OS_PACKET_BUFFER_SIZE];
static uint8_t sequenceBuffer[4];

class OpenSCADAItem {
 private:

public:
  enum DataType {
    NONE,
    BOOLEAN,
    INT32,
    INT64,
    DOUBLE
  };
  DataType _dataType;
  const char * _name;
  boolean _input;
  boolean _output;
  
  boolean _booleanValue;
  int32_t _int32Value;
  float _doubleValue;

  boolean _writePending;
  
  OpenSCADAItem () :
     _dataType ( NONE ),
     _name ( NULL ),
     _input ( false ),
     _output ( false )
  {
  }
  
  void dataType ( const DataType & dataType )
  {
     this->_dataType = dataType;
  }
  
  const DataType & dataType () const
  {
    return this->_dataType;
  }
  
  void clear ()
  {
      this->_dataType = OpenSCADAItem::NONE;
  }
  
  void value ( boolean value )
  {
    this->_dataType = OpenSCADAItem::BOOLEAN;
    this->_booleanValue = value;
  }

  void value ( int value )
  {
    this->value ( (int32_t)value );
  }

  void value ( int32_t value )
  {
    this->_dataType = OpenSCADAItem::INT32;
    this->_int32Value = value;
  }

  void value ( float value )
  {
    this->_dataType = OpenSCADAItem::DOUBLE;
    this->_doubleValue = value;
  }

  void value ( double value )
  {
    this->_dataType = OpenSCADAItem::DOUBLE;
    this->_doubleValue = value;
  }

  void writeRequest ( bool value )
  {
    this->value ( value );
    this->_writePending = true;
  }

  void writeRequest ( int32_t value )
  {
    this->value ( value );
    this->_writePending = true;
  }

  void writeRequest ( float value )
  {
    this->value ( value );
    this->_writePending = true;
  }
  
  boolean booleanValue () const
  {
    return this->_booleanValue;
  }
  
  int32_t int32Value () const
  {
    return this->_int32Value;
  }

  float doubleValue () const
  {
    return this->_doubleValue;
  }
   
  void name ( const char * name )
  {
    this->_name = name;
  }

  const char * name () const
  {
    return this->_name;
  }
  
  void input ( boolean input )
  {
    this->_input = input;
  }
  
  boolean input () const
  {
    return this->_input;
  }

  void output ( boolean output )
  {
    this->_output = output;
  }

  boolean output () const
  {
    return this->_output;
  }

  bool writePending () const
  {
    return this->_writePending;
  }

  bool checkAndResetWrite ()
  {
    bool result = _writePending;
    this->_writePending = false;
    return result;
  }

  boolean booleanWriteRequest ( boolean defaultValue = false )
  {
    switch ( _dataType )
    {
	case NONE:
   		return defaultValue;
	case BOOLEAN:
		return booleanValue ();
	case INT32:
		return int32Value () != 0;
//	case INT64:
//		return int64Value () != 0L;
	case DOUBLE:
		return doubleValue () != 0.0f;
	default:
		return defaultValue;
    }
  }

  int32_t int32WriteRequest ( int32_t defaultValue = 0)
  {
    switch ( _dataType )
    {
	case NONE:
   		return defaultValue;
	case BOOLEAN:
		return booleanValue () ? 1 : 0;
	case INT32:
		return int32Value ();
//	case INT64:
//		return int64Value () != 0L;
	case DOUBLE:
		return doubleValue ();
	default:
		return defaultValue;
    }
  }

  float doubleWriteRequest ( float defaultValue = 0.0f )
  {
    switch ( _dataType )
    {
	case NONE:
   		return defaultValue;
	case BOOLEAN:
		return booleanValue () ? 1.0f : 0.0f;
	case INT32:
		return (float)int32Value ();
//	case INT64:
//		return int64Value () != 0L;
	case DOUBLE:
		return doubleValue ();
	default:
		return defaultValue;
    }
  }
};

template<uint8_t count>
class OpenSCADA {
protected:
  OpenSCADAItem items[count];
  uint8_t inputs;
  uint8_t outputs;
  EthernetUDP & udp;

  // buffers for receiving and sending data
  uint8_t packetBuffer[OS_PACKET_BUFFER_SIZE]; //buffer to hold incoming packet,

public:

  OpenSCADA ( EthernetUDP & udp ) : 
    udp ( udp )
  {
  }

  void setItem ( uint8_t pos, const char * name, boolean input, boolean output )
  {
    if ( pos >= count )
      return; // failure
      
    items[pos].name ( name );
    items[pos].input ( input );
    items[pos].output ( output );
    
    this->inputs = 0;
    this->outputs = 0;
    
    for ( int i = 0; i < count; i++ )
    {
      if ( items[i].input () )
        inputs++;
      if ( items[i].output () )
        outputs++;
    }
  }

  OpenSCADAItem & getItem ( uint8_t pos )
  {
	return items[pos];
  }
  
  void setBooleanValue ( uint8_t pos, boolean value )
  {
    if ( pos >= count )
      return; // failure
      
    items[pos].value ( value );
  }
  
  void setInt32Value ( uint8_t pos, int32_t value )
  {
    if ( pos >= count )
      return; // failure
      
    items[pos].value ( value );
  }

  void setDoubleValue ( uint8_t pos, double value )
  {
    if ( pos >= count )
      return; // failure
      
    items[pos].value ( value );
  }
  
  void clearValue ( uint8_t pos )
  {
    if ( pos >= count )
      return; // failure
      
    items[pos].clear ();
  }
  
private:
  
  void sendData ( IPAddress remoteIp, uint16_t remotePort, byte * seq )
  {
    replyBuffer[7] = 5; // reply code
  
    replyBuffer[8] = inputs;  // number of following entries
    
    // data

    int pos = 9;
    
    for ( int i = 0; i < count; i++ )
    {
      if ( !items[i].input () )
        continue;
        
      switch ( items[i].dataType () )
      {
        case OpenSCADAItem::BOOLEAN:
	{
          replyBuffer[pos++] = 1;
          replyBuffer[pos++] = items[i].booleanValue () ? 0x0FF : 0x00;
	}
          break;
        case OpenSCADAItem::INT32:
        {
          int32_t value = items[i].int32Value ();
          replyBuffer[pos++] = 2;
          replyBuffer[pos++] = value;
          replyBuffer[pos++] = value >> 8;
          replyBuffer[pos++] = value >> 16;
          replyBuffer[pos++] = value >> 24;
        }
          break;
	case OpenSCADAItem::DOUBLE:
	{
	  replyBuffer[pos++] = 4;
	  float value = items[i].doubleValue ();
	  ::memcpy ( replyBuffer+pos, &value, sizeof(float) );
	  pos+=sizeof(float);
	}
	  break;
	
        default:
          replyBuffer[pos++] = 0;
          break;
      }
    }
    
    sendPacket ( replyBuffer, remoteIp, remotePort, seq, pos );
  }

  void sendConfiguration ( IPAddress & remoteIp, uint16_t remotePort, byte * seq )
  {
    replyBuffer[7] = 3;
    replyBuffer[8] = inputs;
    replyBuffer[9] = outputs;

    int pos = 10;
    for ( int i = 0; i < count; i++ )
      {
	if ( items[i].input () || items[i].output () )
		pos = addItemConfiguration ( items[i], i, replyBuffer, pos );
      }
    
    sendPacket ( replyBuffer, remoteIp, remotePort, seq, pos );
  }

  int addItemConfiguration ( const OpenSCADAItem & item, uint8_t signalNumber, byte * buffer, int pos )
  {
    buffer[pos++] = 0x01; // type #1: IO flags
    buffer[pos++] = signalNumber;
    buffer[pos++] = 1; // length
    buffer[pos++] = ( item.input () ? 0x01 : 0x00 ) | ( item.output () ? 0x02 : 0x00 );

    const char * itemName = item.name ();
    int len = strlen ( itemName );
    if ( len > 0xFF )
	len = 0xFF;
    buffer[pos++] = 0x02; // type #2: signal name
    buffer[pos++] = signalNumber;
    buffer[pos++] = len;
    ::memcpy ( buffer + pos, itemName, len );
    pos += len;

    return pos;
  }

  // #define DUMP_RESULT(task,rc) Serial.print ( task  ); Serial.print ( ": " ); Serial.println ( rc );
  #define DUMP_RESULT(task,rc) rc

  void sendPacket ( uint8_t  * buffer, IPAddress & remoteIp, uint16_t remotePort, byte * seq, size_t size  )
  {
    buffer[0] = 0x04;
    buffer[1] = 0xB2;
    buffer[2] = 1;
    
    buffer[3] = seq[0];
    buffer[4] = seq[1];
    buffer[5] = seq[2];
    buffer[6] = seq[3];

    DUMP_RESULT ( "beginPacket", udp.beginPacket ( remoteIp, remotePort ) );
    DUMP_RESULT ( "write", udp.write ( buffer, size ) );
    DUMP_RESULT ( "endPacket", udp.endPacket () );
  }

  byte * getSequence ( byte * packetBuffer )
  {
    sequenceBuffer[0] = packetBuffer[3];
    sequenceBuffer[1] = packetBuffer[4];
    sequenceBuffer[2] = packetBuffer[5];
    sequenceBuffer[3] = packetBuffer[6];
    return sequenceBuffer;
  }

  void handleWriteRequest ( int size, uint8_t * packetBuffer )
  {
	if ( size < 3 )
		return;

	uint16_t pos = packetBuffer[0] | ( packetBuffer[1] << 8 );
	// Serial.println ( pos );

	if ( pos >= count )
		return; // failure

	uint8_t dataType = packetBuffer[2];

	// Serial.println ( dataType );
	
	switch ( dataType )
	{
		case 0:
			// somehow handle NULL write request
			break;
		case 1:
			// setting boolean
			items[pos].writeRequest ( packetBuffer[3] != 0x00 );
			break;
		case 2:
			{
			// setting int32
			int32_t vi = packetBuffer[3] | ( packetBuffer[4] << 8 ) | ( packetBuffer[5] << 16 ) | ( packetBuffer[6] << 24 );
			items[pos].writeRequest ( vi );
			}
			break;
		case 4:
			{
			float vf;
			::memcpy ( &vf, packetBuffer+3, sizeof(float) );
			items[pos].writeRequest ( vf );
			}
			break;
		default:
			// unsupported
			break;
	}
  }

public:
  void handlePacket ( int size, uint8_t * packetBuffer, IPAddress remoteIp, uint16_t remotePort )
  {
    uint8_t commandCode = packetBuffer[7];

    // Serial.println ( commandCode, HEX );
    
    if ( commandCode == 0x02 )
    {
      // Serial.println ( "Request configuration" );
      sendConfiguration ( remoteIp, remotePort, getSequence ( packetBuffer ) );
    }
    else if ( commandCode == 0x04 )
    {
      // Serial.println ( "Request data" );
      sendData ( remoteIp, remotePort, getSequence ( packetBuffer ) );
    }
    else if ( commandCode == 0x06 )
    {
	// Serial.println ( "Request write" );
	handleWriteRequest ( size, packetBuffer + 8 );
    }
  }
  
  void processPackets ()
  {
	// if there's data available, read a packet
	int packetSize = udp.parsePacket(); // note that this includes the UDP header

	if(packetSize)
	{
		IPAddress remote = udp.remoteIP();
		// read the packet into packetBufffer and get the senders IP addr and port number
		int size = udp.read ( packetBuffer, OS_PACKET_BUFFER_SIZE );
		handlePacket ( size, packetBuffer, remote, udp.remotePort () );
	}
  }

};

#endif
